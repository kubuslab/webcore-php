<?php

abstract class FormCacheHandler extends \WebCore\Form\FormCacheHandler {

    function __construct() {
        log_message('debug', 'FormCacheHandler: ' . get_class($this));
    }

    public function editForm($cache, $params = null) : array
    {
        $return = $this->tangani($cache, $params);
        return [$cache, $return];
    }

    public function newForm($cache) : array
    {
        $this->baru($cache);
        return [$cache, true];
    }

	abstract function tangani(&$cache, $params = null);
    function baru(&$cache) { }
}
