<?php
namespace WebCore\Inisiator\CI3;

use WebCore\Inisiator\CI3\Components\Logger;
use WebCore\Inisiator\CI3\Components\Db;
use WebCore\Inisiator\CI3\Components\Cache;
use WebCore\Inisiator\CI3\Components\Password;
use WebCore\Inisiator\CI3\Components\MemObject;
use WebCore\Inisiator\CI3\Components\Session;
use WebCore\Inisiator\CI3\Components\Variable;
use WebCore\Inisiator\CI3\Components\User;
use WebCore\Inisiator\CI3\Components\Permission;
use WebCore\Inisiator\CI3\Components\Form;
use WebCore\Inisiator\CI3\Components\Modevice;
use WebCore\Inisiator\CI3\Components\InisiatorExecutor;
use Exception;
use Web_Controller;
use WS_Controller;

/**
 * Components WebCore::AppContext untuk framework CI 3.x
 */
class Manager
{
	public const INIT_CLASS = '\WebCore\AppContext';
	public const SETUP_NONE = 0;
	public const SETUP_PROGRESS = 1;
	public const SETUP_INVALID = 2;
	public const SETUP_BYPASS = 3;
	public const SETUP_SUCCESS = 4;

	private static $status = self::SETUP_NONE;
	private static $app = NULL;

	private static $_classes = [];

	private static $main = [
		Logger::class => false,
		Db::class => false,
		Cache::class => false,
		Password::class => false,
		MemObject::class => false,
		Session::class => false,
		Variable::class => false,
	];

	private static $mapper = [
		'user' => [ 'class' => User::class, 'active' => true, 'object' => null, 'executor' => false ],
		'permission' => [ 'class' => Permission::class, 'active' => true, 'object' => null ],
		'form' => [ 'class' => Form::class, 'active' => true, 'object' => null ],
		'modevice' => [ 'class' => Modevice::class, 'active' => false, 'object' => null ],
	];

	private static $extend = [
		'datacm' => [ 'class' => DataCM::class, 'active' => true, 'object' => null ],
	];

	private static $subs = [
		'formcm' => [ 'library' => 'form', 'getter' => 'getCacheManager', 'active' => true, 'object' => null ],
	];

	private static $alias = [
		'Datacm/memobjek' => 'memobject',
		'usession' => 'user',
	];

	public static function status()
	{
		return self::$status;
	}

	public static function setup($config = null)
	{
		if (self::$status != self::SETUP_NONE)
			return self::$app;
		if (!self::checkrequired())
		{
			self::$status = self::SETUP_BYPASS;
			return null;
		}

		if (empty($config))
			$config = [];
		if (!isset($config['baseUrl']))
			$config['baseUrl'] = \base_url();
		if (!isset($config['constants']) || !is_array($config['constants']))
			$config['constants'] = [];

		/** @var WS_Controller|Web_Controller */
		$ci =& \get_instance();

		$config['constants'] += [
			'GID_ANONIM' => GID_ANONIM,
			'GID_ADMIN' => GID_ADMIN,
			'GID_REGULER' => GID_REGULER,
			'GROUP_ADMIN' => GROUP_ADMIN,
			'GROUP_REGULER' => GROUP_REGULER,
			'UID_ANONIM' => UID_ANONIM,
			'USER_ANONIM_NAMA' => USER_ANONIM_NAMA,
			'USER_REGULER_NAMA' => USER_REGULER_NAMA,
			'GID_ADMIN_USER' => GID_ADMIN_USER,
			'GROUP_ADMIN_USER' => GROUP_ADMIN_USER,
		];

		try {
			if (isset($config['inisiator']) && $config['inisiator'] == 100) {
				self::$status = self::SETUP_BYPASS;
				return null;
			}

			self::$status = self::SETUP_PROGRESS;

			foreach (self::$main as $clz => $obj) {
				/** @var ComponentsInterface */
				$obj = new $clz;
				self::$main[$clz] = $obj;
				$obj->init($ci, $config);
			}

			//log_message('debug', '==== CONFIG AppContext ==== ' . print_r($config, true));
			//$app = AppContext::instance($config);
			self::app($config);
			//log_message('debug', '----- APP ----' . print_r($app, true));
			self::$app->initialize();

			// set PathFinderInterface
			self::$app->setPathFinder(new PathFinder());

			//log_message('debug', ' ==>  URI: ' . print_r([$ci->router->osegments, $ci->uri->uri_string], true));
			self::$app->setUri([
				!empty($ci->router->osegments) ? '/' . implode('/', $ci->router->osegments) : '/', // letakkan original uri di index-0
				!empty($ci->uri->uri_string) ? '/' . $ci->uri->uri_string : '/',
			]);

			foreach (self::$main as $obj) {
				$obj->prepare($ci, self::$app, $config);
			}
			
			//log_message('debug', '----- APP ----' . print_r($app, true));
			//self::$app->get('log')->debug('system', '---------- INISIALISASI AppContext ----------');
			self::$app->get("events")->attach("system:shutdown", function () /* use ($app) */ {
				// lebih baik pakai Manager::$app
				//log_message('debug', '----- APP ----' . print_r($app, true));
			});

			self::$status = self::SETUP_SUCCESS;
			return self::$app;
		} catch (Exception $e) {
			self::$status = self::SETUP_INVALID;
		}

		return null;
	}

	public static function load($library, $config = [])
	{
		if (self::$status != self::SETUP_SUCCESS)
			return false;

		if (isset(self::$alias[$library]))
			$library = self::$alias[$library];

		if (self::$app->has($library))
		{
			$ci =& \get_instance();
			if (self::$app->isLoaded($library))
			{
				if (isset(self::$mapper[$library]['class']) && self::$mapper[$library]['active']) {
					if (!isset(self::$mapper[$library]['object']))
					{
						$clz = self::$mapper[$library]['class'];
						$obj = new $clz;
						if ($obj instanceof InisiatorExecutor && isset (self::$mapper[$library]['executor']) && self::$mapper[$library]['executor'] === true)
							$obj->prepareExecutor($ci, self::$app, $config);
						self::$mapper[$library]['object'] = $obj;
					}
				}
			}
			elseif (isset(self::$mapper[$library]['class']) && self::$mapper[$library]['active'])
			{
				if (!isset(self::$mapper[$library]['object']))
				{
					$clz = self::$mapper[$library]['class'];
					$obj = new $clz;
					$obj->init($ci, $config);
					$obj->prepare($ci, self::$app, $config);
					if ($obj instanceof InisiatorExecutor && isset (self::$mapper[$library]['executor']) && self::$mapper[$library]['executor'] === true)
						$obj->prepareExecutor($ci, self::$app, $config);
					self::$mapper[$library]['object'] = $obj;
				}
			}
			else
				return false;

			$lib = self::$app->get($library);

			// insert manual formcm
			if ($library == 'form' && self::$mapper['form']['active'] && !isset(self::$mapper['formcm'])) {
				self::$mapper['formcm'] = ['active' => true, 'object' => $lib->getCacheManager() ];
			}

			log_message('debug', ' ==== AMBIL LIBRARY DARI WebCore: ' . $library/*  . ' --> ' . print_r($lib, true) */);
			return $lib;
		}
		elseif (isset(self::$extend[$library]) && self::$extend[$library]['active'])
		{
			if (!isset(self::$extend[$library]['object']))
			{
				$clz = self::$extend[$library]['class'];
				$obj = new $clz;
				self::$extend[$library]['object'] = $obj;
			}
			
			log_message('debug', ' ==== AMBIL LIBRARY DARI WebCore:Extend: ' . $library);
			return self::$extend[$library]['object'];
		}
		elseif (isset(self::$subs[$library]) && self::$subs[$library]['active'])
		{
			$clz = self::$subs[$library]['library'];
			$getter = self::$subs[$library]['getter'];

			if (!isset(self::$subs[$library]['object'])) {
				// pastikan induk library dimuat terlebih dahulu
				$parent = self::load($clz);

				$obj = self::$mapper[$clz]['object'];
				self::$subs[$library]['object'] = $parent->$getter();
			}
			
			log_message('debug', ' ==== AMBIL LIBRARY DARI WebCore:SubLibrary: ' . $clz . '->' . $getter . '() : ' . $library);
			return self::$subs[$library]['object'];
		}
		// Internally loaded by WebCore
		elseif (self::$app->isLoaded($library))
		{
			$lib = self::$app->get($library);
			log_message('debug', ' ==== AMBIL LIBRARY DARI WebCore: ' . $library);
			return $lib;
		}

		return false;
	}

	public static function loadExecutor($library, $config = [])
	{
		if (self::$status != self::SETUP_SUCCESS)
			return false;

		if (isset(self::$alias[$library]))
			$library = self::$alias[$library];

		if (isset(self::$mapper[$library]['object']) && self::$mapper[$library]['object'] instanceof InisiatorExecutor) {
			$ci =& \get_instance();
			self::$mapper[$library]['object']->prepareExecutor($ci, self::$app, $config);
		}
	}

	public static function component($library)
	{
		if (self::$status != self::SETUP_SUCCESS)
			return null;

		if (isset(self::$alias[$library]))
			$library = self::$alias[$library];

		if (isset(self::$mapper[$library]))
			return self::$mapper[$library]['object'];
		elseif (isset(self::$extend[$library]))
			return self::$extend[$library]['object'];

		return null;
	}

	public static function app($config = null)
	{
		if (self::$status == self::SETUP_NONE)
			throw new Exception('Inisiator manager belum di-setup', 20);

		if (self::$status == self::SETUP_PROGRESS)
		{
			if (is_array($config) && count($config) > 0)
				self::$app = call_user_func_array([self::INIT_CLASS, 'instance'], [$config]);
			else
				self::$app = call_user_func(self::INIT_CLASS, 'instance');
		}
		
		return self::$app;
	}

	public static function getConstant($key)
	{
		if (self::$status != self::SETUP_SUCCESS)
			throw new Exception('Manager tidak menggunakan Inisator WebCore', 21);

		return self::$app->constant($key);
	}

	public static function setConstant($key, $value)
	{
		if (self::$status != self::SETUP_SUCCESS)
			return false;

		self::$app->setConstant($key, $value);
		return true;
	}

	public static function &loadClass($ci, $class, $folder, $suffix, $module = null, $excludes = array())
	{
		if (isset(self::$_classes[$folder][$class]))
			return self::$_classes[$folder][$class];

		$name = str_replace(' ', '', ucwords(str_replace('_', ' ', $class))) . $suffix;

		if (!class_exists($name)) {
			if (!in_array($name, $excludes)) {
				if ($path = $ci->load->cari($folder . '/', $class . '.php', $module)) { // handle HMVC
					require $path;
				}
			}
		}
		
		if (!class_exists($name))
			throw new \Exception("Class $name tidak ditemukan di $class.php", 121);

		self::$_classes[$folder][$class] = new $name();
		return self::$_classes[$folder][$class];
	}

	private static function checkrequired()
	{
		$ok = class_exists(self::INIT_CLASS);

		return $ok;
	}
}
