<?php
namespace WebCore\Inisiator\CI3;

use DateTime;
use DateTimeZone;

class WebController extends \CI_Controller {
	/**
	 * @var DateTimeZone
	 */
	public $serverTimezone;

	/**
	 * @var DateTime
	 */
	public $serverTime;

	/**
	 * @var string
	 */
	public $serverOffsetText;

	/**
	 * @var DateTimeZone
	 */
	public $clientTimezone;

	/**
	 * @var \Messages
	 */
	public $messages;

	/**
	 * @var \Session
	 */
	public $session;

	/**
	 * @var \Usession
	 */
	public $usession;
	
	/**
	 * @var \Theme
	 */
	public $theme;

	/**
	 * @var \Permission
	 */
	public $permission;

	/**
	 * @var \Variable
	 */
	public $variable;
	
	/**
	 * @var \Web_Hooks
	 */
	public $syshooks;

	/**
	 * @var \Web_CustomHooks
	 */
	public $hooks;

	/**
	 * @var \Memobjek
	 */
	public $memobjek;

	/**
	 * @var \Features
	 */
	public $features;
	
	/**
	 * @var \Menu
	 */
	public $menu;
	
	/**
	 * @var \Profiler
	 */
	public $profiler;

	//region Helper Object Auto-Complete untuk IDE
	/**
	 * Object Helper Autocomplete untuk Web_Loader
	 * @var WebLoader
	*/
	public $load;

	/**
	 * Object Helper Autocomplete untuk CI_DB_query_builder
	 * @var \CI_DB_query_builder
	 */
	public $db;

	/**
	 * Object Helper Autocomplete untuk Web_Config
	 * @var WebConfig
	 */
	public $config;

	/**
	 * Object Helper Autocomplete untuk Web_Router
	 * @var WebRouter
	 */
	public $router;

	/**
	 * Object Helper Autocomplete untuk CI_URI
	 * @var \CI_URI
	 */
	public $uri;

	/**
	 * Object Helper Autocomplete untuk CI_Output
	 * @var \CI_Output
	 */
	public $output;

	/**
	 * Object Helper Autocomplete untuk CI_Input
	 * @var \CI_Input
	 */
	public $input;

	/**
	 * Object Helper Autocomplete untuk CI_Security
	 * @var \CI_Security
	 */
	public $security;

	/**
	 * Object Helper Autocomplete untuk CI_Lang
	 * @var \CI_Lang
	 */
	public $lang;

	/**
	 * Object Helper Autocomplete untuk CI_Utf8
	 * @var \CI_Utf8
	 */
	public $utf8;
	//endregion

	var $noajax = FALSE;
	var $controllerType = 'WEB';

	public function __construct() {
		parent::__construct();
		log_message('debug', "\n\n\t>> MENGAKSES WEB " . $this->router->fetch_directory() . '' . $this->router->fetch_class() . '/' . $this->router->fetch_method() . " " . $this->router->request_method() . " <<\n\nCWD: " . getcwd());
	
		try {
			if ($this->noajax) {
				$input	=& load_class('Input', 'core');
				if ($input->is_ajax_request() || $input->get_request_header('Content-Type') == 'application/json') {
					// load theme engine seketika
					$this->load->library('theme');
					$this->theme->init();
					$this->theme->renderer->addCachedJavascript('var base_url = "' . base_path() . '";');

					throw new \HttpException('Tidak diperbolehkan mengakses konten melalui Ajax', 131, 500);
				}
			}

			// TAMBAHKAN KEMAMPUAN DATA CACHE MANAGER
            $this->load->library('datacm');

			// masukkan juga System Hooks
			$this->syshooks =& load_class('Hooks', 'core');

			// siapkan custom hooks untuk berbagai kebutuhan
			$this->hooks =& load_class('CustomHooks', 'core');
			
			////// mulai proses bootstrap //////
			// ORM Profiler
			$this->load->library('profiler/Profiler', 'profiler');

			// siapkan modul variable
			$this->load->library('variable');
			
			// load theme engine
			$this->loadTheme();
			
			// karena sess_update membutuhkan library datacm dan datacm membutuhkan variable, maka load session disini
			$this->load->library('session');
			$this->session->start();

			// load MemObjek sebenarnya usdah dilakukan di session, ini hanya opsional saja
			$this->load->library('Datacm/memobjek', NULL, 'memobjek');
			
			// load user session
			$this->load->library('usession');
			$this->usession->init();

			// load features
			$this->load->library('features');
			$this->features->build();

			// load permission inspector
			$this->load->library('permission');
			// inspeksi user permission
			$this->permission->inspect('user');
			
			// load menu builder
			$this->load->library('menu');

			// jalankan unhooks, System Hooks dan Custom Hooks
			$this->syshooks->unhook();
			$this->hooks->unhook();

			//region EXPERIMENTAL BELUM BISA DIGUNAKAN SEKARANG
			// $debug = config_item('debug_profiler');
			// log_message('debug', '>>>> DEBUG PROFILER: ' . $debug);
			// $this->output->enable_profiler($debug === TRUE);
			//endregion
		} catch (\Exception $e) {
			// tanggapi error nanti di controller fungsional spesifik
			//ERROR Session time out
			//ERROR Tidak diperbolehkan mengakses resource
			//ERROR daftar permission tidak ditemukan
			//ERROR dan lain-lain
			$this->tanggapi_error($e);
		}
	}

	public function show_error()
	{
		$this->backlink_title = 'Beranda';
		$this->backlink_path = '';
		$this->_404_heading = '404 Page Not Found';
		$this->_404_message = 'The page you requested was not found.';

		$this->load->config('webexception', FALSE, FALSE, null, TRUE);
		$cfg = $this->config->item('webexception');
		foreach (array('backlink_title', 'backlink_path', '_404_heading', '_404_message') as $key) {
			if (isset($cfg[$key]))
				$this->$key = $cfg[$key];
		}

		// renderer->show_error langsung melakukan prosedur exit
		$this->theme->renderer->show_error($this->_404_message, 404, base_path($this->backlink_path), $this->_404_heading, $this->backlink_title, 'error_404');
	}

	/**
	 * 
	 * @param Exception $e
	 */
	protected function tanggapi_error($e) {
		$this->theme->renderer->show_error($e, $e->getCode(), '', 'Error', 'Beranda');
	}
	
	public function _remap($method, $arguments) {
		try {
			// is_callable() returns TRUE on some versions of PHP 5 for private and protected
			 // methods, so we'll use this workaround for consistent behavior
			 if ( ! in_array(strtolower($method), array_map('strtolower', get_class_methods($this))))
			 {
				 $this->load->library('router');
				 // Check and see if we are using a 404 override and use it.
				 if ( ! empty($this->router->routes['404_override']))
				 {
					 $x = explode('/', $this->router->routes['404_override']);
					 $class = $x[0];
					 $method = (isset($x[1]) ? $x[1] : 'index');
					 if ( ! class_exists($class))
					 {
						 if ( ! file_exists(APPPATH.'controllers/'.$class.'.php'))
						 {
							 show_404("{$class}/{$method}");
						 }

						 include_once(APPPATH.'controllers/'.$class.'.php');
						 $CI = new $class();
						 call_user_func_array(array(&$CI, $method), $arguments);
					 }
				 }
				 else
				 {
					 show_404($this->router->fetch_class()."/{$method}");
				 }
			 }else {
				// Call the requested method.
				// Any URI segments present (besides the class/function) will be passed to the method for convenience
				call_user_func_array(array(&$this, $method), $arguments);
			 }
		} catch (\Exception $e) {
			$this->tanggapi_error($e);
		}
	}

	public function manager($fungsi, $params = array())
	{
		if (Manager::status() != Manager::SETUP_SUCCESS)
			return NULL;

		if (is_array($params) && count($params) > 0)
			call_user_func_array(array(Manager::class, $fungsi), $params);
		else
			return call_user_func(Manager::class, $fungsi);
	}

	public function loadTheme() {
		// load theme engine
		$this->load->library('theme');
		$this->theme->init();
		$this->theme->renderer->addCachedJavascript('var base_url = "' . base_path() . '";');
	}
}
