<?php
namespace WebCore\Inisiator\CI3;

class WebRouter extends \MX_Router {

	/**
	 * @var array Original Segments, before located
	 */
	var $osegments;

	/**
	 * @var string original uri, reconstruct from $osegments
	 */
	var $ouri;

	/**
	 * Set route mapping
	 *
	 * Determines what should be served based on the URI request,
	 * as well as any "routes" that have been set in the routing config file.
	 *
	 * @return	void
	 */
	protected function _set_routing()
	{
		/// EDIT MANUAL MENAMBAHKAN PERAN DOMAIN DALAM MENENTUKAN config routes.php
		$file_path = \get_config_path('routes');
		if (file_exists($file_path))
			include $file_path;

		// Validate & get reserved routes
		if (isset($route) && is_array($route))
		{
			isset($route['default_controller']) && $this->default_controller = $route['default_controller'];
			isset($route['translate_uri_dashes']) && $this->translate_uri_dashes = $route['translate_uri_dashes'];
			unset($route['default_controller'], $route['translate_uri_dashes']);
			$this->routes = $route;
		}

		// Are query strings enabled in the config file? Normally CI doesn't utilize query strings
		// since URI segments are more search-engine friendly, but they can optionally be used.
		// If this feature is enabled, we will gather the directory/class/method a little differently
		if ($this->enable_query_strings)
		{
			// If the directory is set at this time, it means an override exists, so skip the checks
			if ( ! isset($this->directory))
			{
				$_d = $this->config->item('directory_trigger');
				$_d = isset($_GET[$_d]) ? trim($_GET[$_d], " \t\n\r\0\x0B/") : '';

				if ($_d !== '')
				{
					$this->uri->filter_uri($_d);
					$this->set_directory($_d);
				}
			}

			$_c = trim($this->config->item('controller_trigger'));
			if ( ! empty($_GET[$_c]))
			{
				$this->uri->filter_uri($_GET[$_c]);
				$this->set_class($_GET[$_c]);

				$_f = trim($this->config->item('function_trigger'));
				if ( ! empty($_GET[$_f]))
				{
					$this->uri->filter_uri($_GET[$_f]);
					$this->set_method($_GET[$_f]);
				}

				$this->uri->rsegments = array(
					1 => $this->class,
					2 => $this->method
				);
			}
			else
			{
				$this->_set_default_controller();
			}

			// Routing rules don't apply to query strings and we don't need to detect
			// directories, so we're done here
			return;
		}

		// Is there anything to parse?
		if ($this->uri->uri_string !== '')
		{
			$this->_parse_routes();
		}
		else
		{
			$this->_set_default_controller();
		}
	}

	protected function _set_request($segments = array())
	{
		log_message('debug', '[OURI: ' . implode('/', $segments) . ', URI: ' . $this->uri->uri_string . ']');
		$this->osegments = $segments;
		$this->ouri = '/' . implode('/', $segments);

		if (count($segments) > 0) {
			/* lihat potensi bahwa ini adalah route ke user_homepage */
			$this->_user_routes();
		}

		if ($this->translate_uri_dashes === TRUE)
		{
			foreach(range(0, 2) as $v)
			{
				isset($segments[$v]) && $segments[$v] = str_replace('-', '_', $segments[$v]);
			}
		}
		
		$segments = $this->locate($segments);

		if($this->located == -1)
		{
			/* [TAMBAHAN] Jika route controller tidak ditemukan maka asumsikan bahwa ini mungkin adalah 'user home page' */
			if (!$this->userpage || !($segments = $this->_user_home_page($this->osegments)))
			{
				$this->_set_404override_controller();
				return;
			}
		}

		if(empty($segments))
		{
			$this->_set_default_controller();
			return;
		}
		
		$this->set_class($segments[0]);
		
		if (isset($segments[1]))
		{
			$this->set_method($segments[1]);
		}
		else
		{
			$segments[1] = 'index';
		}
       
		array_unshift($segments, NULL);
		unset($segments[0]);
		$this->uri->rsegments = $segments;
	}

	private function _user_routes() {
		$file_path = \get_config_path('user_homepage');
		if (file_exists($file_path))
			include $file_path;

		$this->userroutes = ( ! isset($userroutes) OR ! is_array($userroutes)) ? array() : $userroutes;
		unset($userroutes);

		/* [TAMBAHAN] Siapkan konfigurasi untuk menangani 'user home page' */
		if (isset($this->userroutes['user_homepage']) && $this->userroutes['user_homepage'] === TRUE) {
			if (isset($this->userroutes['user_homepage_domain']) && count($this->userroutes['user_homepage_domain']) > 0) {
				$this->userpage_domain = $this->userroutes['user_homepage_domain'];
				$this->userpage = TRUE;
				unset($this->userroutes['user_homepage_domain']);
			}else
				$this->userpage = FALSE;

			unset($this->userroutes['user_homepage']);
		}else
			$this->userpage = FALSE;
	}

	/**
	 * Method ini digunakan untuk mengasumsikan bahwa 'user home page' dihandle oleh controller tertentu
	 * sesuai dengan mapping domain.
	 *
	 * Sebagai contoh misalkan didefinisian $routes['user_homepage_domain']['dev.kubuslab.com'] = 'demo/home/page'
	 * 
	 * Jika 'demo' adalah directory module, 'home' adalah class controller dan 'page' adalah method yang ada didalmanya,
	 * maka URI yang baru menjadi 'demo/home/page/segment_0/segment_1/.../segment_n'
	 *
	 * CATATAN: Kesepakatan segments[0] adalah username
	 */
	private function _user_home_page($segments) {
		//print 'Test';print_r(array($this->userroutes, $this->userpage_domain));exit;
		$input =& load_class('Input', 'core');
		$host = HTTP_HOST;
		if (isset($this->userpage_domain[$host])) {
			if ($baru = $this->locate(explode('/', $this->userpage_domain[$host])))
				return array_merge($baru, $segments); // segmen baru disisipkan diawal array segments
		}
	}

	public function request_method() {
		$CI =& \get_instance();
		return strtolower($CI->input->server('REQUEST_METHOD'));
	}

	/** Locate the controller **/
	public function locate($segments) {
		$return = parent::locate($segments);

		if (!defined('MODULE')) {
			define('MODULE', $this->module);
			define('MODULEPATH', FCPATH . 'modules/' . $this->module . '/');

			if (!empty($this->module)) {
				/*// load config modules.php pada domain terkait
				$host = HTTP_HOST;
				if (!empty($host) && defined('ENVIRONMENT') && is_file(APPPATH.'config/domains/'.$host.'/'.ENVIRONMENT.'/modules.php')) {
					include(APPPATH.'config/domains/'.$host.'/'.ENVIRONMENT.'/modules.php');
				} elseif (!empty($host) && is_file(APPPATH.'config/domains/'.$host.'/modules.php')) {
					include(APPPATH.'config/domains/'.$host.'/modules.php');
				}*/
				$file_path = \get_config_path('modules');
				if (file_exists($file_path))
					include $file_path;

				if (isset($modules) && (!isset($modules['allow_all']) || $modules['allow_all'] === FALSE) && isset($modules['allows'])) {
					if (is_array($modules['allows']) && !in_array($this->module, $modules['allows'])) {
						//define('MODULE_DENIED', TRUE);
						show_error('Halaman atau modul tidak tersedia untuk anda', 500, 'Galat!');
					}
				}
			}
		}

		return $return;
	}
}