<?php
namespace WebCore\Inisiator\CI3;

use WebCore\Cache;
use WebCore\Storage\Adapter\AbstractAdapter;

class DataCM
{
	/** @var Cache */
	private $cache;

	private $_settings = array(
		'default_prefix' => 'WebCore/',
	);

	function __construct() {
		$this->_initialize();
	}

	/**
	 * Get 
	 *
	 * Look for a value in the cache.  If it exists, return the data 
	 * if not, return FALSE
	 *
	 * @param 	string	
	 * @return 	mixed		value that is stored/FALSE on failure
	 */
	public function get($id, $default = NULL, $notfound = NULL) {
		if (!$this->_check()) return FALSE;
		$key = $this->_getRealKey($id);

		$data = $this->cache->get($key, $default);
		if ($data != AbstractAdapter::RESOURCE_NOTFOUND) {
			return $data;
		}

		if (isset($notfound) && is_callable($notfound)) return $notfound($key, $default);
		return $default;
	}

	public function getAll($filter = NULL) {
		if (!$this->_check()) return FALSE;

		$array_key = $this->semua_key();
		if (empty($array_key)) {
			return array();
		}

		if ($filter === TRUE)
			$prefix = $this->_getRealKey('');
		$data = array();
		foreach ($array_key as $realkey) {
			if ($filter === TRUE && strpos($realkey, $prefix) !== 0)
				continue;
			$nilai = $this->ambil('../' . $realkey); // ambil secara realkey
			if ($nilai !== FALSE)
				$data[$realkey] = $nilai;
		}

		return $data;
	}

	public function getKeys($prefix = NULL) {
		if (!$this->_check()) return FALSE;
		return $this->cache->getKeys($prefix);
	}

	// ------------------------------------------------------------------------

	/**
	 * Cache Save
	 *
	 * @param 	string		Unique Key
	 * @param 	mixed		Data to store
	 * @param 	int			Length of time (in seconds) to cache the data
	 *
	 * @return 	boolean		true on success/false on failure
	 */
	public function set($id, $data, $ttl = null) {
		if (!$this->_check()) return FALSE;
		return $this->cache->set($id, $data, $ttl);
	}

	// ------------------------------------------------------------------------

	/**
	 * Delete from Cache
	 *
	 * @param 	mixed		unique identifier of the item in the cache
	 * @return 	boolean		true on success/false on failure
	 */
	public function delete($id) {
		if (!$this->_check()) return FALSE;
		return $this->cache->delete($id);
	}

	// ------------------------------------------------------------------------

	/**
	 * Clean the cache
	 *
	 * @return 	boolean		false on failure/true on success
	 */
	public function clear() {
		if (!$this->_check()) return FALSE;
		return $this->cache->clear();
	}

	// ------------------------------------------------------------------------

	/**
	 * Cache Info
	 *
	 * @param 	string		user/filehits
	 * @return 	mixed		array on success, false on failure	
	 */
	public function info($type = 'user') {
		return FALSE;
	}

	// ------------------------------------------------------------------------

	/**
	 * Close connection
	 */
	public function close($type = null) {
		return FALSE;
	}

	// ------------------------------------------------------------------------

	/**
	 * Initialize
	 *
	 * Initialize class properties based on the configuration array.
	 *
	 * @param	array 	
	 * @return 	void
	 */
	private function _initialize() {
		$cache = Manager::load('cache');
		if (!$cache)
			return false;
		$this->cache = $cache;

		$ci =& get_instance();
		$ci->load->config('cache');
		$config = $ci->config->item('cache');
		if (isset($config['settings']))
			$this->_settings = $config['settings'] + $this->_settings;

		// setup default_prefix
		if (isset($this->_settings['host_prefix'])) {
			$host = HTTP_HOST;
			foreach ($this->_settings['host_prefix'] as $h => $p) {
				// cari apakah ditemukan host_prefix sesuai dengan host
				$pattern = strtr(preg_quote($h, '#'), array('\*' => '.*', '\?' => '.'));
				if (preg_match('#^' . $pattern . '$#', $host)) {
					$this->_settings['default_prefix'] = $p;
					break;
				}
			}

		}

		log_message('debug', "Init Datacm:" . get_class($cache->getAdapter()));
	}

	// ------------------------------------------------------------------------

	/**
	 * Is the requested driver supported in this environment?
	 *
	 * @param 	string	The driver to test.
	 * @return 	array
	 */
	public function is_supported() {
		if ($this->cache && $this->cache->getAdapter())
			return $this->cache->getAdapter()->isSupported();
		return false;
	}
	
	private function _check() {
		if (!isset($this->cache)) {
			log_message('debug', 'Library WebCore\Cache tidak tersedia');
			return FALSE;
		}

		return TRUE;
	}
	
	private function _getRealKey($id, $prefix = NULL) {
		if (isset($prefix))
			return $prefix.$id;

		if (substr($id, 0, 3) == '../')
			return substr($id, 3);

		if (isset($this->_settings[$id]['prefix']))
			$prefix = $this->_settings[$id]['prefix'];
		else{
			// jika tidak ada maka gunakan default_prefix
			if (!isset($prefix))
				$prefix = $this->_settings['default_prefix'];
		}

		return $prefix.$id;
	}
}
