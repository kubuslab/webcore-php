<?php
namespace WebCore\Inisiator\CI3;

use Web_Controller;

class WebHooks extends \CI_Hooks {
    protected $modul;
    protected $pendirian = 'system';
    protected $system_hooks = array(
        'pre_system', 'cache_override', 
        'pre_controller', 'post_controller_constructor', 
        'post_controller', 'display_override', 'post_system',
    );

    private static $internal = array(
        'init_session' => 'session:init',
        'update_session_id' => 'session:updateId',
        'destroy_session_id' => 'session:destroy',
        'user_session' => 'session:user',
        'saat_login_sukses' => 'session:onLoginSuccess',
        'sebelum_logout' => 'session:beforeLogout',
        'validasi_login' => 'session:validateLogin',
        'saat_login_gagal' => 'session:onLoginFailed',
        //'setelah_login' => 'session:afterLogin',
        'ketika_user_memuat_session' => 'session:onUserLoadSession',
    );

    public function _initialize($modul = NULL) {
        $this->modul = $modul;
        if (empty($modul))
            $this->__initialize();
        elseif ($this->enabled) {
            //log_message('debug', 'LOAD HOOKS AWAL ' . print_r($this->hooks, true));
            list($path, $_file) = \Modules::find('hooks.php', $modul, 'config/');
			if ($path != FALSE)
				include $path . $_file;
            
            if ( ! isset($hook) OR ! is_array($hook))
            {
                return;
            }

            //log_message('info', 'LOAD HOOKS ' . print_r($hook, true) . "\nTRACE:\n" . print_r(debug_backtrace(0), true));
            $this->_perbaiki_path_hooks($hook, $modul);

            foreach ($this->hooks as $k => $v) {
                if (isset($hook[$k])) {
                    $this->hooks[$k] = array_merge ($v, $hook[$k]);
                    unset($hook[$k]);
                }
            }
            foreach ($hook as $k => $v) {
                $this->hooks[$k] = $v;
            }

            //log_message('debug', 'LOAD HOOKS AKHIR ' . print_r($this->hooks, true));
        }

        $this->prepareInternal();
    }

    /**
     * Initialize the Hooks Preferences. MENSIMULASI CI_Hooks::_initialize dengan penambahan pencarian config pada domain
     *
     * @access  private
     * @return  void
     */
    private function __initialize() {
        $CFG =& load_class('Config', 'core');

        // If hooks are not enabled in the config file
        // there is nothing else to do

        if ($CFG->item('enable_hooks') == FALSE)
        {
            return;
        }

        $file_path = get_config_path('hooks');
        if (file_exists($file_path))
            include $file_path;


        if ( ! isset($hook) OR ! is_array($hook))
        {
            return;
        }

        $this->hooks =& $hook;
        $this->enabled = TRUE;
    }

    protected function prepareInternal()
    {
        if (Manager::status() != Manager::SETUP_SUCCESS)
            return;

        static $sudah = array();
        $manager = Manager::app()->get('events');
        foreach (self::$internal as $k => $h) {
            if (!isset($sudah[$k]) && isset($this->hooks[$k])) {
                log_message('debug', 'MIGRARASI HOOKS ' . $k . ' => ' . $h);
                $hooks = $this->hooks[$k];
                foreach ($hooks as $hook) {
                    $this->_run_internal_hook($manager, $hook, $h);
                }
                unset($this->hooks[$k]);
                $sudah[$k] = true;
            }
        }
    }

    protected function _run_internal_hook($manager, $hook, $name)
    {
        $that = $this;
        $manager->attach($name, function($event, $source, $data = null) use($that, $hook) {
            if (!empty($data))
                $hook['params'] = $data;
            //$that->jalankanLangsung($hook);

            // karena filepath tiap hook sudah diperbaiki maka langsung saja eksekusi
            $that->_run_hook($hook);
        });
    }

    /**
     *  Method ini dipanggil dari dalam constructor Web_Controller atau WS_Controller
     */
    public function unhook() {
        // UNHOOK yang bukan sistem
        $this->_unhook_non_system();

        list($path, $_file) = \Modules::find('unhooks.php', $this->modul, 'config/');
        if ($path === FALSE)
            return;

        include $path . $_file;

        $CI =& get_instance();
        $uri = trim($CI->uri->uri_string(), '/');
        $ruri = trim($CI->uri->ruri_string(), '/');
        if (!isset($unhooks))
            return;
        log_message('debug', 'UNHOOK RURI:' . $ruri . ' <--> URI:' . $uri . ' [' . $this->modul . ']');
        foreach ($unhooks as $key => $value) {
            if (strstartswith($uri, $key) == 0 || strstartswith($ruri, $key) == 0) {
                foreach ($value as $h => $qualifications) {
                    foreach ($qualifications as $qualification) {
                        $this->removeHook($h, $qualification);
                    }
                }
            }
        }

        //log_message('info', 'HOOKS Tersedia untuk ' . ucfirst($this->pendirian) . ' Hooks -> ' . print_r($this->hooks, TRUE));
    }

    /**
     *  Menghapus hook sesuai spek array $qualification pada hook $key. hook yang akan
     *  dihapus adalah hanya hook pertama yang ditemukan.
     *  @param string $key nama hook
     *  @param array $qualification array berisi kualifikasi hook yang akan dihapus 
     */
    public function removeHook($key, $qualification) {
        if (!isset($this->hooks[$key]) || empty($this->hooks[$key]))
            return;

        foreach ($this->hooks[$key] as $k => $v) {
            $ok = TRUE;
            foreach ($qualification as $q1 => $q2) {
                if (isset($v[$q1]) && $q2 != $v[$q1]) {
                    $ok = FALSE;
                    break;
                }
            }
            if ($ok) {// semua qualification sesuai
                unset($this->hooks[$key][$k]); // hapus hook
                log_message('info', 'UNHOOKS ' . ucfirst($this->pendirian) . "Hooks=$key, Class=" . $v['filename']);
                break;
            }
        }
    }

    /**
     * Jika $modul ditentukan memungkinkan meminjam hooks dari modul tersebut.
     * Jika tidak maka filepath akan dicari pada APPPATH dan modulpath
     * @param string $key nama hook
     * @param array $hook array konfigurasi hook
     * @param int $insert posisi hook akan disisipkan
     * @param string modul nama modul external termpat hook terletak
     */
    public function tambahHook($key, $hook, $insert = FALSE, $modul = NULL) {
        if (!isset($this->hooks[$key]) || !isset($hook['filepath']) || !isset($hook['filename'])) // jika belum didefinisikan maka hentikan
            throw new \Exception("Menambahkan HOOK $key yang invalid. " . print_r($hook, true), 305);

        if (!isset($modul)) {
            $this->_redefinisi_modul();
            if (isset($hook['module']) && !empty($hook['module']))
                $modul = $hook['module'];
            else {
                $modul = $this->modul;
                $filepath = APPPATH.$hook['filepath'].'/'.$hook['filename'];
            }
        }
        
        if ( !isset($filepath) || ! file_exists($filepath)) {
            if ($path = $this->_cari_folder($hook['filepath'], $modul)) {
                $filepath = APPPATH.$path.'/'.$hook['filename'];
                $hook['filepath'] = $path;
            }else
                throw new \Exception("HOOK '$key' file $filepath tidak ditemukan", 304);
        }

        if (!file_exists($filepath))
            throw new \Exception("HOOK '$key' file $filepath tidak ditemukan", 304);
        if ($insert !== FALSE) {// insert diakhir
            $this->hooks[$key][] = $hook;
        }else {
            $jl = count($this->hooks);
            if ($insert >= $jl) $insert--;
            else if ($insert <= -$jl) $insert++;
            array_splice($this->hooks[$key], $insert, 0, array($hook));
        }
    }

    public function jalankanLangsung($hook, $modul = NULL) {
        if (!isset($hook['filepath']) || !isset($hook['filename'])) { // jika belum didefinisikan maka hentikan
            log_message('debug', "Menjalankan Langsung HOOK yang invalid. " . print_r($hook, true));
            throw new \Exception("Menjalankan Langsung HOOK yang invalid. " . print_r($hook, true), 301);
        }
        if (!isset($modul)) {
            $this->_redefinisi_modul();
            if (isset($hook['module']) && !empty($hook['module']))
                $modul = $hook['module'];
            else {
                $modul = $this->modul;
                $filepath = APPPATH.$hook['filepath'].'/'.$hook['filename'];
            }
        }

        if ( !isset($filepath) || ! file_exists($filepath)) {
            if ($path = $this->_cari_folder($hook['filepath'], $modul)) {
                $filepath = APPPATH.$path.'/'.$hook['filename'];
                $hook['filepath'] = $path;
            }else {
                log_message('debug', "Menjalankan Langsung HOOK $filepath di modul '$modul' tidak ditemukan");
                throw new \Exception("Menjalankan Langsung HOOK $filepath di modul '$modul' tidak ditemukan", 302);
            }
        }

        if (!file_exists($filepath)) {
            log_message('debug', "Menjalankan Langsung HOOK $filepath di modul '$modul' tidak ditemukan");
            throw new \Exception("Menjalankan Langsung HOOK $filepath di modul '$modul' tidak ditemukan", 302);
        }

        return $this->_run_hook($hook);
    }
    
    protected function _perbaiki_path_hooks(&$hooks, $modul) {
        
        foreach ($hooks as $k => $v) {
            if (!is_array($v)) continue;
            foreach ($v as $k1 => $h) {
                if (isset($h['filename']) && isset($h['filepath'])) {
                    $mod = isset($h['module']) ? $h['module'] : $modul;
                    if ($path = $this->_cari_folder($h['filepath'], $mod)) {
                        $hooks[$k][$k1]['filepath'] = $path;
                    }else {
                        log_message('debug', ' ***** TIDAK Ditemukan dir ' . $h['filepath'] . '@' . $mod);
                        unset($hooks[$k][$k1]);
                    }
                }
            }
        }
    }
    
    protected function _cari_folder($base, $modul) {
        static $_cfolder = array();
        $key = $base . '/' . ($modul ? $modul : '');
        if (isset($_cfolder[$key]))
            return $_cfolder[$key];

        if (empty($modul)) {
            // print_r([$base, $modul, FCPATH, APPPATH]);exit;
            $dir = APPPATH.$base;
            if (is_dir($dir)) {
                $source = $base;
                $_cfolder[$key] = $source;
                return $source;
            }

            return FALSE;
        }

        foreach (\Modules::$locations as $location => $offset) {
            if (is_dir($location.$modul.'/' . $base)) {
                $source = $offset.$modul.'/' . $base;
                log_message('debug', ' ----- Ditemukan dir ' . $source);
                if (substr($source, 0,3) == '../')
                    $source = substr ($source, 3);

                $_cfolder[$key] = $source;
                return $source;
            }
        }

        return FALSE;
    }

    protected function _redefinisi_modul() {
        $CI =& get_instance();
        if (empty($this->modul) && isset($CI->router))
            $this->modul = $CI->router->fetch_module();
    }

    protected function _unhook_non_system() {
        if ($this->pendirian != 'system')
            return;

        // Hapus Semua Non-System Hooks, agar tidak terjadi tabrakan kerja
        foreach (array_keys($this->hooks) as $hook) {
            $ketemu = FALSE;
            foreach ($this->system_hooks as $key) {
                if ($hook == $key) {
                    $ketemu = TRUE;
                    break;
                }
            }

            if (!$ketemu) {
                unset($this->hooks[$hook]);
            }
        }
        
    }
}
