<?php
namespace WebCore\Inisiator\CI3;

class WebConfig extends \MX_Config {

	/**
	 * List of all loaded merger config files
	 * @var array
	 */
	private $is_loaded_merge = array();

	public function __construct() {
		parent::__construct();

		// MUAT SUBCONSTANT
		include_once \get_config_path('subconstants');

		// jika ada pengalihan protokol dari HTTP ke HTTPS menggunakan HTTP_X_FORWARDED_PROTO (Biasanya dilakukan oleh HAPROXY), segera layanani
		// tapi tidak melayani pengalihan dari HTTPS ke HTTP
		if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
			$httpson = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off';
			if (!$httpson) {
				log_message('debug', "Pengalihan protokol dari HTTP ke HTTPS karena HTTP_X_FORWARDED_PROTO");
				$base_url = 'https://'. HTTP_HOST;
				$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

				$this->set_item('base_url', $base_url);
			}
		}
	}

	/** SUDAH DIMODIFIKASI DENGAN MENAMBAHKAN PARAMETER $_all
	*
	*/
	public function load($file = 'config', $use_sections = FALSE, $fail_gracefully = FALSE, $_module = '', $all = FALSE) {
		//debug_print_backtrace(0);
		//print "[" . $file . '] All ' . $all . "\n";
		if (in_array($file, $this->is_loaded, TRUE)) return $this->item($file);

		$ci_exists = class_exists('CI');
		if ($ci_exists) {
			$_module OR $_module = \CI::$APP->router->fetch_module();
			list($path, $file) = \Modules::find($file, $_module, 'config/');
		}else {
			$path = FALSE;
		}
		
		if ($path === FALSE) {
			$this->_load($file, $use_sections, $fail_gracefully);
			return $this->item($file);
		}
		
		// muat juga config di APPPATH
		if ($all) {
			$this->_load($file, $use_sections, $fail_gracefully);
		}
		
		if ($config = \Modules::load_file($file, $path, 'config')) {
			/* reference to the config array */
			$current_config =& $this->config;

			if ($use_sections === TRUE)	{
				
				if (isset($current_config[$file])) {
					$current_config[$file] = array_replace_recursive($current_config[$file], $config);
				} else {
					$current_config[$file] = $config;
				}
				
			} else {
				$current_config = array_replace_recursive($current_config, $config);
			}
			$this->is_loaded[] = $file;
			unset($config);
			return $this->item($file);
		} else {
			if ($fail_gracefully === TRUE) {
				return FALSE;
			}
			show_error('Your '.$path.$file.' file does not appear to contain a valid configuration array.');
		}

	}

	public function merge($file = 'config', $use_sections = FALSE, $fail_gracefully = FALSE, $_module = '', $path = FALSE)
	{
		$key = $file . '@' . ($_module ? $_module : '');
		if (in_array($key, $this->is_loaded_merge, TRUE)) return $this->item($file);

		if ($path === FALSE)
			list($path, $file) = \Modules::find($file, $_module, 'config/');

		if ($config = \Modules::load_file($file, $path, 'config')) {
			/* reference to the config array */
			$current_config =& $this->config;

			if ($use_sections === TRUE)	{
				if (isset($current_config[$file])) {
					$current_config[$file] = array_replace_recursive($current_config[$file], $config);
				} else {
					$current_config[$file] = $config;
				}
			} else {
				$current_config = array_replace_recursive($current_config, $config);
			}
			$this->is_loaded_merge[] = $key;
			unset($config);
			return $this->item($file);
		} else {
			if ($fail_gracefully === TRUE) {
				return FALSE;
			}
			show_error('Your '.$path.$file.' file does not appear to contain a valid configuration array.');
		}
	}

	public function find($file = '')
	{
		$file = ($file == '') ? 'config' : str_replace('.php', '', $file);
		$found = FALSE;
		$loaded = FALSE;

		/* $host = HTTP_HOST;
		$check_locations = defined('ENVIRONMENT')
			? ( !empty($host) ? array(
					'domains/'.$host.'/'.ENVIRONMENT.'/'.$file,
					'domains/'.$host.'/'.$file,
					ENVIRONMENT.'/'.$file,
					$file
				) : array(
					ENVIRONMENT.'/'.$file,
					$file
				)
			) : ( !empty($host) ? array(
					'domains/'.$host.'/'.$file,
					$file
				) : array(
					$file
				)
			); */

        $check_locations = \get_config_path($file, TRUE);
		foreach ($this->_config_paths as $path)
		{
			foreach ($check_locations as $location)
			{
				$file_path = $path.'config/'.$location.'.php';

				if (in_array($file_path, $this->is_loaded, TRUE))
				{
					$loaded = TRUE;
					continue 2;
				}

				if (file_exists($file_path))
				{
					$found = TRUE;
					break;
				}
			}

			if ($found === TRUE)
			{
				return $file_path;;
			}
		}

		return NULL;
	}

	/**
	 * Load Config File dimabil dari CI_Config::load dengan beberapa penyesuaian
	 *
	 * @access	public
	 * @param	string	the config file name
	 * @param   boolean  if configuration values should be loaded into their own section
	 * @param   boolean  true if errors should just return false, false if an error message should be displayed
	 * @return	boolean	if the file was loaded correctly
	 */
	private function _load($file = '', $use_sections = FALSE, $fail_gracefully = FALSE)
	{
		$file = ($file == '') ? 'config' : str_replace('.php', '', $file);
		$found = FALSE;
		$loaded = FALSE;

		$check_locations = \get_config_path($file, TRUE);
		if (count($check_locations) > 0)
		{
			foreach ($this->_config_paths as $path)
			{
				foreach ($check_locations as $location)
				{
					$file_path = $path.'config/'.$location; //.'.php';

					if (in_array($file_path, $this->is_loaded, TRUE))
					{
						$loaded = TRUE;
						continue 2;
					}

					if (file_exists($file_path))
					{
						$found = TRUE;
						break;
					}
				}

				if ($found === FALSE)
				{
					continue;
				}

				include($file_path);

				if ( ! isset($config) OR ! is_array($config))
				{
					if ($fail_gracefully === TRUE)
					{
						return FALSE;
					}
					show_error('Your '.$file_path.' file does not appear to contain a valid configuration array.');
				}

				if ($use_sections === TRUE)
				{
					if (isset($this->config[$file]))
					{
						//$this->config[$file] = array_merge($this->config[$file], $config);
						$this->config[$file] = array_replace_recursive($this->config[$file], $config);
					}
					else
					{
						$this->config[$file] = $config;
					}
				}
				else
				{
					//$this->config = array_merge($this->config, $config);
					$this->config = array_replace_recursive($this->config, $config);
				}

				$this->is_loaded[] = $file_path;
				unset($config);

				$loaded = TRUE;
				log_message('debug', 'Config file loaded: '.$file_path);
				break;
			}
		}

		if ($loaded === FALSE)
		{
			if ($fail_gracefully === TRUE)
			{
				return FALSE;
			}
			show_error('The configuration file '.$file.'.php does not exist.');
		}

		return TRUE;
	}
}
