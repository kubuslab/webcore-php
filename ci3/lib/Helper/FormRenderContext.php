<?php
namespace WebCore\Inisiator\CI3\Helper;

use WebCore\Form\RendererInterface;
use WebCore\Inisiator\CI3\Manager;

class FormRenderContext implements RendererInterface
{
    private $origin;
    private $module;
    private $theemLoaded = false;

    public function __construct()
    {
        $CI =& \get_instance();
        $this->origin = $this->module = $CI->load->getCurrentModule();
    }

    public function switchModule(string $module = NULL)
    {
        // FIXED: BOLEHKAN SWITCH KE MODULE YANG SAMA AGAR PEMANGGILAN FORM-HALAMAN BISA DILAKUKAN
        log_message('debug', "FORMRENDER: MEMULAI SWITCH MODULE '" . $this->origin . "' -> '" . $module . "'");
        /* if ($this->origin == $module)
            return; */
        
        // simpan modul target
        $this->module = $module;

        /** @var WS_Controller */
        $CI =& \get_instance();
        $module = $CI->load->switch_module($module);

        /** @var \WebCore\Inisiator\CI3\Components\Form */
        $inisiatorForm = Manager::component("form");
        $inisiatorForm->setupAll($CI, null, $module);

        return $module;
    }

    public function switchBackModule()
    {
        if ($this->origin != $this->module)
        {
            /** @var WS_Controller */
            $CI =& \get_instance();
            $CI->load->switch_module($this->module);
        }
    }

    public function bootFromModule(string $module = null)
    {
        log_message('debug', 'FORMRENDER: MEMULAI REBOOT DARI MODULE');
        /** @var WS_Controller */
        $CI =& \get_instance();
		$CI->load->rebootFromModule();
    }

    public function createView(string $content, $cache, $module = null)
    {
        $this->loadTheme();

        log_message('debug', 'FORMRENDER: MEMULAI CREATE VIEW');
        $CI =& \get_instance();
        $konten = $CI->theme->renderer->createStaticContent($content, $cache, $module);
        return $konten;
    }

    private function loadTheme()
    {
        if ($this->theemLoaded)
            return;

        /** @var WS_Controller */
        $CI =& \get_instance();

        // set helper untuk cache
		$CI->load->helper('fc');

		// load theme engine
		$CI->load->library('theme');
        $CI->theme->init();

        $this->theemLoaded = true;
    }
}
