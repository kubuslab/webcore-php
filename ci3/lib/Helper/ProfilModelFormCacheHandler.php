<?php

use WebCore\Form\FormCacheHandler;

class ProfilModelFormCacheHandler extends FormCacheHandler {
	public function tangani(&$cache, $params = null) {
		log_message('info', "Handling Cache Form " . $this->manager->formid . " dengan ProfilModelFormCacheHandler");
		/* @var $CI WS_Controller */
		$CI =& get_instance();
		$id = $params['id'];
		unset($params['id']);
		$CI->profiler->setModul($this->manager->modul);
		$model = $CI->profiler->init($this->manager->model);
		$model->muat($id, true);
		if ($model->apaBerhasilDimuat()) {
			$cache['id'] = $model->id;
			$array = $model->toArray('FormCache');

			foreach ($model->partner_fields as $field) {
				// ## METHOD 1: SUDAH TIDAK DIPAKAI LAGI
				//if (isset($model->partner_info->$field))
				//	$cache['cache'][$field] = $model->partner_info->$field;

				// ## METHOD 2: ambil nilai dari array
				if (isset($array[$field])) {
					$cache['cache'][$field] = $array[$field];
					unset($array[$field]);
				}
			}
			
			//$model->muatSemuaRelasi();
			//$kontenRelasi = $model->getRelasi();
			$kontenRelasi = array();
			if (isset($array['#relasi'])) {
				log_message('debug', 'KONTEN RELASI -> ' . print_r($kontenRelasi, TRUE));
				$kontenRelasi = $array['#relasi'];
				unset($array['#relasi']);
			}
			foreach ($kontenRelasi as $relasi => $konten) {
				if (is_array($konten))
					$cache['cache'][$relasi] = $konten;
			}

			// Handle Relasi Eksternal yg diperoleh dari Join
			if (isset($array['#eksternal'])) {
				log_message('debug', 'RELASI EKSTENAL -> ' . print_r($array['#eksternal'], TRUE));
				$cache['cache']['__eksternal'] = $array['#eksternal'];
				unset($array['#eksternal']);
			}
			
			// Sebagai tambahan masukkan spesifikasi yg disebutkan pada toArray
			foreach ($array as $key => $value) {
				$cache['cache'][$key] = $value;
			}
			
			return true;
		}
		
		return false;
	}
}