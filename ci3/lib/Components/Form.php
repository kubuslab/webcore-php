<?php
namespace WebCore\Inisiator\CI3\Components;

use WebCore\Inisiator\CI3\Manager;
use WebCore\Inisiator\CI3\Helper\FormRenderContext;

class Form implements InisiatorInterface
{
	private $prepared = false;
	private $config = [];
	private $formBuilder;

	public function init($ci, &$config, $module = '')
	{
		$ci->load->config('forms', FALSE, FALSE, $module, TRUE);
		$config['form'] = [
			'salt' => $ci->config->item('forms_pengacau'),
			'multipleToken' => $ci->config->item('forms_multiple_token'),
			'handlers' => $ci->config->item('forms_cache_handler'),
			'plugins' => $ci->config->item('forms_render_handler'),
			'mapping' => $ci->config->item('forms_cache_mapping'),
			'permissions' => $ci->config->item('forms_perms_handler'),
			
		];

		$remoteCache = $ci->config->item('forms_cache_pakai_datacm');
		$config['form']['remoteCache'] = !isset($remoteCache) || $remoteCache === TRUE;

		$serializer = $ci->config->item('forms_serializer');
		$config['form']['serializer'] = !empty($serializer) ? $serializer : 'Json';
	}

	public function prepare($ci, $app, $config)
	{
		if (!isset($config['form']) && !is_array($config['form']))
			return;

		if (!$this->prepared)
		{
			// Siapkan Proxy class agar tetap compatible dengan CI 3.x WebCore 2
			require_once dirname(__FILE__) . '/../../others/FormCacheHandler.php';

			// set helper untuk cache
			$ci->load->helper('fc');

			// simpan config untuk digunakan kemudian
			$this->config = $config['form'];

			// simpan form builder
			$formContext = new FormRenderContext();
			$this->formBuilder = $app->get('form', [$config['form']]);
			$this->formBuilder->setViewRenderer($formContext);
			$this->prepared = true;
		}
		else
		{
			// simpan config untuk digunakan kemudian
			$this->config = $config['form'];
			$this->formBuilder->setConfig($config['form']);
		}
	}

	public function setupAll($ci, $formid = null, $module = '')
	{
		$app = Manager::app();
		$config = [];
		$this->init($ci, $config, $module);
        $this->prepare($ci, $app, $config);

		if (empty($formid))
		{
			$cform = $app->get("form")->getCurrentForm();
			if ($cform && isset($cform['id'])) {
				$this->setup($ci, $cform['id']);
			}
		}
		else
		{
			$this->setup($ci, $formid);
		}
	}

	public function setup($ci, $formid)
	{
		//$cform = $this->form->getUserForms($formid);
		$this->setupHandler($ci, $formid);
		$this->setupPermission($ci, $formid);
	}

	public function setupPermission($ci, $formid)
	{
		// bangun permission ispector untuk form
		$permission = Manager::app()->get("permission");
		if (isset($this->config['permissions']) && is_array($this->config['permissions']) && isset($this->config['permissions'][$formid]))
		{
			$perms = $this->config['permissions'][$formid];
			foreach ($perms as $perm)
			{
				// #1 Pakai Manager::loadClass
				// $inspektor = Manager::loadClass($ci, $perm, 'permissions', 'Permission');
				// $permission->addInspector($perm, $inspektor);

				// #2 Langusng pakai WebLoader::permission dengan inspect=false
				$ci->load->permission($perm, false);
			}
		}
	}

	public function setupHandler($ci, $formid)
    {
		$uforms = $this->formBuilder->getUserForms();
		if (!isset($uforms[$formid]))
			throw new \Exception('Form ID tidak ditemukan', 141);

		// bangun plugin form
		if (is_array($this->config['plugins']) && isset($this->config['plugins'][$formid]))
		{
			$renderer = $this->config['plugins'][$formid];
			// foreach ($this->config['plugins'] as $formid => $renderer)
			// {
				if (isset($renderer['handler']))
				{
					$handler = Manager::loadClass($ci, $renderer['handler'], 'frenderers', 'FormRenderer', $uforms[$formid]['modul']);
					$renderer['handler'] = $handler;
					$this->formBuilder->addPlugin($formid, $renderer);
				}
				else
					throw new \Exception('Form ' . $formid . ' membutuhkan definisi plugin renderer', 600);
					
			// }
		}

		// bangun cache handler form
		if (is_array($this->config['handlers']) && isset($this->config['handlers'][$formid]))
		{
			$conf = $this->config['handlers'][$formid];
			// foreach ($this->config['handlers'] as $formid => $conf)
			// {
				if (isset($conf['handler']))
				{
					if ($conf['handler'] != 'default')
						$handler = Manager::loadClass($ci, $conf['handler'], 'fchandlers', 'FormCacheHandler', isset($conf['modul']) ? $conf['modul'] : $uforms[$formid]['modul'], ['DefaultFormCacheHandler']);
					else
						$handler = 'default';
					$this->formBuilder->addHandler($formid, $handler);
				}
				else
					throw new \Exception('Form ' . $formid . ' membutuhkan cache handler', 600);
			// }
		}
		//endregion

		
    }
}
