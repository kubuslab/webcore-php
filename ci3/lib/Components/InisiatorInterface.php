<?php
namespace WebCore\Inisiator\CI3\Components;

interface InisiatorInterface
{
    public function init($ci, &$config);
    public function prepare($ci, $app, $config);
}