<?php
namespace WebCore\Inisiator\CI3\Components;

class Db implements InisiatorInterface
{
    public function init($ci, &$config)
    {
        // siapkan config database
        $file_path = \get_config_path('database');
        include($file_path);
        if (isset($db) && count($db) > 0)
        {
            if (!isset($active_group))
                $active_group = 'default';

            foreach ($db as $k => $opsi) {
                if (!isset($opsi['dbdriver']) || !in_array($opsi['dbdriver'], ['mysql', 'mysqli', 'pdo', 'postgre', 'sqlite', 'sqlite3']))
                    continue;
                if ($opsi['dbdriver'] == 'mysql' || $opsi['dbdriver'] == 'mysqli')
                    $driver = 'mysql';
                elseif ($opsi['dbdriver'] == 'postgre')
                    $driver = 'postgresql';
                elseif ($opsi['dbdriver'] == 'sqlite' || $opsi['dbdriver'] == 'sqlite3')
                    $driver = 'sqlite';
                elseif ($opsi['pdo'] && in_array($opsi['subdriver'], ['mysql', 'pgsql', 'sqlite']))
                    $driver = $opsi['subdriver'] == 'pgsql' ? 'postgresql' : $opsi['subdriver'];
                else
                    continue;

                unset($opsi['dbdriver'], $opsi['subdriver']);
                
                //rename field
                $rename = [
                    'hostname' => 'host',
                    'port' => 'port',
                    'username' => 'username',
                    'password' => 'password',
                    'database' => 'dbname',
                    'char_set' => 'charset',
                    'pconnect' => 'persistent',
                    'options' => 'options',
                ];
                $opsi2 = [];
                foreach ($rename as $o => $v)
                {
                    if (isset($opsi[$o]))
                    {
                        $opsi2[$v] = $opsi[$o];
                        unset($opsi[$o]);
                    }
                }
                
                if ($k == $active_group) {
                    $config['db'] = ['adapter' => $driver] + $opsi2;
                    // agar tidak memberatkan sistem hanya active_group saja yang diaktifkan 
                    break;
                }else
                    $config['db:' . $k] = ['adapter' => $driver] + $opsi2;
            }
        }
    }

    public function prepare($ci, $app, $config)
    {
        # code...
    }
}
