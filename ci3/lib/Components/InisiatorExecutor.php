<?php
namespace WebCore\Inisiator\CI3\Components;

interface InisiatorExecutor
{
    public function prepareExecutor($ci, $app, $config);
}