<?php
namespace WebCore\Inisiator\CI3\Components;

class Logger implements InisiatorInterface
{
    public function init($ci, &$config)
    {
        // NOTHING TODO
    }

    public function prepare($ci, $app, $config)
    {
        $ci->load->config('logger');
        $config = $ci->config->item('logger');
        $app->getLog()->addHandler(function($channel, $level, $message) use ($config) {
            // $channel dan $level bisa di disabled dari config
            if (!isset($config['channels'][$channel]) || is_bool($config['channels'][$channel]) && $config['channels'][$channel] === TRUE ||
                is_array($config['channels'][$channel]) && (!isset($config['channels'][$channel][$level]) 
                || is_bool($config['channels'][$channel][$level]) && $config['channels'][$channel][$level] === TRUE))
                log_message($level, '[' . strtoupper($channel) . '] -> ' . $message);
        });
    }
}
