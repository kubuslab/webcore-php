<?php
namespace WebCore\Inisiator\CI3\Components;

class Permission implements InisiatorInterface
{
    private $prepared = false;

    public function init($ci, &$config)
    {
        // NOTHING TODO
    }

    public function prepare($ci, $app, $config)
    {
        if (!$this->prepared)
        {
            // Siapkan Proxy class agar tetap compatible dengan CI 3.x WebCore 2
            require_once dirname(__FILE__) . '/../../others/PermissionInspector.php';
            $this->prepared = true;
        }
    }
}
