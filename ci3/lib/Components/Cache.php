<?php
namespace WebCore\Inisiator\CI3\Components;

class Cache implements InisiatorInterface
{
    public function init($ci, &$config)
    {
        $this->initCache($ci, $config);
    }

    public function prepare($ci, $app, $config)
    {
        # code...
    }

    private function initCache($ci, array &$config)
    {
        $ci->load->config('cache');

        // siapkan config cache
        $cache = $ci->config->item('cache');
        if (isset($cache['adapter']) && isset($cache[$cache['adapter']]) && in_array($cache['adapter'], ['apc', 'memcache', 'memcached', 'redis', 'memory', 'stream']) && (!isset($cache['enabled']) || $cache['enabled'] == 1))
        {
            $config['cache']['adapter'] = $cache['adapter'];
            $config['cache']['format'] = !isset($cache['format']) || !in_array($cache['format'], ['json', 'jsonarray', 'php', 'base64', 'igbinary', 'msgpack', 'none']) ? 'jsonarray' : $cache['format'];
            $config['cache'] += $cache[$cache['adapter']]; // options ada disini
        }

        // siapkan config localcache
        $local = $ci->config->item('localcache');
        if (isset($local['adapter']) && isset($local[$local['adapter']]) && in_array($local['adapter'], ['apc', 'memory', 'stream']) && (!isset($local['enabled']) || $local['enabled'] == 1))
        {
            $config['localcache']['adapter'] = $local['adapter'];
            $config['localcache']['format'] = !isset($local['format']) || !in_array($local['format'], ['json', 'jsonarray', 'php', 'base64', 'igbinary', 'msgpack', 'none']) ? 'jsonarray' : $local['format'];
            $config['localcache'] += $local[$local['adapter']]; // options ada disini
        }
    }
}
