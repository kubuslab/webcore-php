<?php
namespace WebCore\Inisiator\CI3\Components;

class Password implements InisiatorInterface
{
    public function init($ci, &$config)
    {
        $ci->load->config('password');
        $password = $ci->config->item('password');
        if ($password && isset($password['default']))
        {
            $config['password']['adapter'] = $password['default'];
            unset($password['default']);
            $config['password']['adapters'] = $password;
        }
    }

    public function prepare($ci, $app, $config)
    {
        // NOTHING TODO
    }
}
