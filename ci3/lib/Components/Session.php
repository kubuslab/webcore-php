<?php
namespace WebCore\Inisiator\CI3\Components;

class Session implements InisiatorInterface
{
    public function init($ci, &$config)
    {
        $keys = [
            'sess_encrypt_cookie',
            'sess_content_cookie',
            'sess_use_database',
            'sess_use_datacm_shadow',
            'sess_table_name',
            'sess_expiration',
            'sess_expire_on_close',
            'sess_match_ip',
            'sess_match_useragent',
            'sess_cookie_name',
            'cookie_prefix',
            'cookie_path',
            'cookie_domain',
            'cookie_secure',
            'cookie_httponly',
            'sess_time_to_update',
            'sess_change_on_update',
            'encryption_key',
            'flashdata_key',
            'time_reference',
            'sess_run_gc',
            'gc_probability',
            'tooexpire',
            'sess_headers',
        ];
        foreach ($keys as $key)
        {
            $config['session'][$key] = $ci->config->item($key);
        }
    }

    public function prepare($ci, $app, $config)
    {
        // NOTHING TODO
    }
}
