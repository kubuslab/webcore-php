<?php
namespace WebCore\Inisiator\CI3\Components;

class Modevice implements InisiatorInterface
{
    public function init($ci, &$config)
    {
        $ci->load->config('mo');
        $config['modevice'] = [
            'activation' => $ci->config->item('mo_activation'),
            'db_versions' => $ci->config->item('mo_db_version'),
            'options' => $ci->config->item('mo_config'),
        ];
    }

    public function prepare($ci, $app, $config)
    {
        if (isset($config['modevice']['options']))
        {
            // cari path eksekutor agar bisa diload otomatis oleh Modevice
            foreach ($config['modevice']['options'] as $key => $conf) {
                if (isset($conf['eksekutor']))
                {
                    if ($path = $ci->load->cari('modevice/', $conf['eksekutor'] . '.php', isset($conf['module']) ? $conf['module'] : null))
                    {
                        $config[$key]['path'] = $path;
                    }
                }
            }
        }
    }
}
