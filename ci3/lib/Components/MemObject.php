<?php
namespace WebCore\Inisiator\CI3\Components;

class MemObject implements InisiatorInterface
{
    public function init($ci, &$config)
    {        
        $ci->config->load('memobjek', FALSE, FALSE, NULL, TRUE);
        $conf = $ci->config->item('memobjek');
        $config['memobject'] = $conf;

    }

    public function prepare($ci, $app, $config)
    {
        # code...
    }
}
