<?php
namespace WebCore\Inisiator\CI3\Components;

class Variable implements InisiatorInterface
{
    public function init($ci, &$config)
    {
        $ci->load->config('variable');
        $config['variable'] = $ci->config->item('variable');
        if (!isset($config['variable']['dirasi']) || $config['variable']['durasi'] <= 0)
            $config['variable']['durasi'] = $config['session']['sess_expiration'];
    }

    public function prepare($ci, $app, $config)
    {
        // NOTHING TODO
    }
}
