<?php
namespace WebCore\Inisiator\CI3\Components;

use WebCore\Inisiator\CI3\Manager;
use WebCore\Session\Executor\ByUsername;
use WebCore\Session\Executor\ByOnlineId;
use WebCore\Session\Executor\ByUsernameOrOnlineId;

class User implements InisiatorInterface, InisiatorExecutor
{
    private static $internal = [
        'by_username' => ByUsername::class,
        'by_online_id' => ByOnlineId::class,
        'by_username_or_online_id' => ByUsernameOrOnlineId::class,
    ];

    public function init($ci, &$config)
    {
        // NOTHING TODO
    }

    public function prepare($ci, $app, $config)
    {
        // NOTHING TODO
    }

    public function prepareExecutor($ci, $app, $config)
    {
        $user = $app->get("user");
        if (!$user->getExecutor())
        {
            $ci->load->config('usession');
            $conf = $ci->config->item('usession');
            $eksekutor = $conf['eksekutor_nama'];
            require_once dirname(__FILE__) . '/../../others/UserPermissionException.php';
            if (isset(self::$internal[$eksekutor])) {
                $clz = self::$internal[$eksekutor];
                $executor = new $clz;
                log_message('debug', 'EKSEKUTOR USESSION: ' . get_class($executor));
            }else {
                $modul = isset($conf['eksekutor_modul']) ? $conf['eksekutor_modul'] : NULL;
                require_once dirname(__FILE__) . '/../../others/SessionEksekutor.php';
                $executor = Manager::loadClass($ci, $eksekutor, 'usession', '', $modul);
            }
            $user->setExecutor($executor);
        }
    }
}
