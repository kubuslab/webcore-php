<?php
namespace WebCore\Inisiator\CI3;

class WebLoader extends \MX_Loader {
	
	//private $nama_theme;
	public function __construct() {
		parent::__construct();
		
		$CI =& \get_instance();
		
		$this->inisiator = config_item('config_inisiator');
		
		if ($CI->controllerType == 'WEB') {
			$this->prepare_view_path();
		}
	}

	public function getCurrentModule() {
		return $this->_module;
	}

	public function rebootFromModule($module = NULL)
	{
		$modul_asli = $this->_module;
		if(!empty($module)) {
			// JIKA SAMA TIDAK PERLU REBOOT
			if ($module == $modul_asli)
				return;
			$this->switch_module($module);
		}else
			$module = $modul_asli;

		list($path, $_file) = \Modules::find('hooks.php', $module, 'config/');
		if ($path != FALSE)
			include $path . $_file;
		if (isset($hook['post_controller_constructor'])) {
			foreach ($hook['post_controller_constructor'] as $h) {
				\CI::$APP->hooks->jalankanLangsung($h, isset($h['module']) ? NULL : $module);
			}
		}

		return $modul_asli;
	}
	
	public function initialize($controller = NULL) {
		parent::initialize($controller);
		
		Manager::setup($this->inisiator);
		
		// inisialisasi Hooks untuk kedua kalinya, untuk menangani hooks khusus pada modul
		$hooks =& \load_class('Hooks', 'core');
		$hooks->_initialize($this->_module);

		// inisialisasi CustomHooks untuk kedua kalinya, untuk menangani hooks khusus pada modul
		$customhooks =& \load_class('CustomHooks', 'core');
		$customhooks->_initialize($this->_module);

		// Load Driver Template Engine (pada Controller utama, untuk antisipasi jika ada NotFoundExceptionController dimuat setelah controller utama)
		\CI::$APP->load->driver('tengine');
	}
	
	public function view($view, $vars = array(), $return = FALSE, $module = null) {
		if (!isset($module) || empty($module))
			$module = $this->_module;
		else {
			// pindah konteks module ke module yang disebutkan secara explisit
			$tmp_module = $this->_module;
			$this->_module = $module;
		}

		// Untuk throw exception, kemungkinan WebLoader::view() dipanggil tanpa proses WebLoader::initialize()
		if (!isset(\CI::$APP->tengine))
			\CI::$APP->load->driver('tengine');
		$usetengine = \CI::$APP->tengine->config('active');
		$exttengine = \CI::$APP->tengine->config('fileext');
		$exts = array('');
		$ext = pathinfo($view, PATHINFO_EXTENSION);
		// untuk php tidak perlu pakai TEngine
		if ($ext == 'php') {
			$usetengine = FALSE;
		}elseif ($usetengine && empty($ext)) {
			$exts = array('', EXT, ".$exttengine");
		}
		
		$search = array('views/', 'themes/global/', '');
		if (isset($this->theme->renderer)) {
			// array_unshift($search, 'themes/' . $this->theme->renderer->nama_res . '/');
			$this->theme->renderer->setViewPaths($search);
		}
		//log_message('debug', "SEARCH PATH " . print_r($search, TRUE));
		foreach ($search as $base) {
			foreach ($exts as $ext) {
				list($path, $_view) = \Modules::find($view.$ext, $module, $base);
				if ($path != FALSE) {
					//log_message('info', "Ditemukan View $path -> $_view");
					$this->_ci_view_paths = array($path => TRUE) + $this->_ci_view_paths;
					$view = $_view;
					break;
				}
			}

			if ($path != FALSE) break;
		}
		
		$return = $this->_ci_load(array('_ci_view' => $view, '_ci_vars' => $this->_ci_prepare_view_vars($vars), '_ci_return' => $return, '_ci_ext' => $exts));
		// kembalikan konteks module ke module semula
		if(isset($tmp_module))
			$this->_module = $tmp_module;
		return $return;
	}
	
	public function library($library = '', $params = NULL, $object_name = NULL, $module = NULL) {
		
		if (!is_array($library)) {
			// jika sudah ada tidak perlu cari lagi
			$class = strtolower(basename($library));
			if (isset($this->_ci_classes[$class]) && !empty($this->_ci_classes[$class]))
				return $this;

			// cek apakah termasuk dalam library native WebCore
			$lib = Manager::load($library);
			if ($lib) {
				($_alias = strtolower($object_name)) OR $_alias = $class;
				\CI::$APP->$_alias = $lib;
				
				$this->_ci_classes[$class] = $_alias;
				return $this;
			}
		}

		// pindah konteks module ke module yang disebutkan secara explisit
		if (isset($module)) {
			$tmp_module = $this->_module;
			$this->_module = $module;
		}
		$return = parent::library($library, $params, $object_name);
		// kembalikan konteks module ke module semula
		if (isset($tmp_module)) {
			$this->_module = $tmp_module;
		}
		return $return;
	}

	public function driver($library, $params = NULL, $object_name = NULL, $module = NULL) {
		if (is_array($library)) {
			foreach ($library as $key => $value) {
				if (is_int($key))
					$this->driver($value, $params);
				else
					$this->driver($key, $params, $value);
			}

			return $this;
		}
		elseif (empty($library))
			return FALSE;

		// cek apakah termasuk dalam library native WebCore
		$lib = Manager::load($library);
		if (!$lib) {
			// CARA STANDAR CI
			if ( ! class_exists('CI_Driver_Library', FALSE)) {
				// We aren't instantiating an object here, just making the base class available
				require BASEPATH.'libraries/Driver.php';
			}

			// We can save the loader some time since Drivers will *always* be in a subfolder,
			// and typically identically named to the library
			if ( ! strpos($library, '/')) {
				$library = ucfirst($library).'/'.$library;
			}
		}

		return $this->library($library, $params, $object_name);
	}
	
	public function config($file = 'config', $use_sections = FALSE, $fail_gracefully = FALSE, $module = null, $all = FALSE) {
		// pindah konteks module ke module yang disebutkan secara explisit
		if (isset($module)) {
			$tmp_module = $this->_module;
			$this->_module = $module;
		}
		
		$return = \CI::$APP->config->load($file, $use_sections, $fail_gracefully, $this->_module, $all);
		
		// kembalikan konteks module ke module semula
		if (isset($tmp_module)) {
			$this->_module = $tmp_module;
		}
		
		return $return;
	}
	
	public function helper($helper = array(), $module = null) {
		// pindah konteks module ke module yang disebutkan secara explisit
		if (isset($module)) {
			$tmp_module = $this->_module;
			$this->_module = $module;
		}
		$return = parent::helper($helper);
		// kembalikan konteks module ke module semula
		if (isset($tmp_module)) {
			$this->_module = $tmp_module;
		}
		
		return $return;
	}

	public function plugin($plugin, $module = null)	{
		// pindah konteks module ke module yang disebutkan secara explisit
		$nama = isset($module) ? $module .'-'. $plugin : $plugin;
		if (isset($module)) {
			$tmp_module = $this->_module;
			$this->_module = $module;
		}
		
		if (is_array($plugin)) return $this->plugins($plugin);		
		
		if (isset($this->_ci_plugins[$nama]))	
			return;

		list($path, $_plugin) = \Modules::find($plugin, $this->_module, 'plugins/');	
		
		if ($path === FALSE AND ! is_file($_plugin = APPPATH.'plugins/'.$_plugin.EXT)) {	
			show_error("Unable to locate the plugin file: {$_plugin}");
		}

		\Modules::load_file($_plugin, $path);
		$this->_ci_plugins[$nama] = TRUE;

		// kembalikan konteks module ke module semula
		if (isset($tmp_module)) {
			$this->_module = $tmp_module;
		}
	}
	
	public function language($langfile = array(), $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '', $module = null) {
		if (isset($module)) {
			$tmp_module = $this->_module;
			$this->_module = $module;
		}
		
		$return = parent::language($langfile, $idiom, $return, $add_suffix, $alt_path);
		
		// kembalikan konteks module ke module semula
		if (isset($tmp_module)) {
			$this->_module = $tmp_module;
		}
		
		return $return;
	}

	public function data($vars = array())
	{
		if (!is_array($vars))
			return;

		foreach ($vars as $k => $v) {
			$this->_ci_cached_vars[$k] = $v;
		}
	}

	public function unset_vars($vars = array()) {
		if (!is_array($vars))
			return;
		foreach (array_keys($vars) as $v) {
			unset($this->_ci_cached_vars[$v]);
		}
	}
	
	public function cari($base, $class, $modul = null) {
		// Improvment untuk setiap pencarian yang ditemukan akan di cache
		// agar tidak menghabiskan waktu untuk mencari hal yang sama
		static $cached = array();
		$key = ($modul ? $modul : '') . ':' . $base . ':' . $class;
		if (isset($cached[$key]))
			return $cached[$key];

		if (substr($base, -1) != '/')
			$base .= '/';
		
		$path = APPPATH . $base . $class;
		if (file_exists($path)) {
			$cached[$key] = $path;
			return $path;
		}
		
		$modul = !empty($modul) ? $modul : $this->_module;
		list($path, $_file) = \Modules::find($class, $modul, $base);
		if ($path != FALSE) {
			$cached[$key] = $path . $_file;
			return $path . $_file;
		}
	}

	public function cari_view($view, $module = null) {
		if (!isset($module))
			$module = $this->_module;
		else {
			// pindah konteks module ke module yang disebutkan secara explisit
			$tmp_module = $this->_module;
			$this->_module = $module;
		}

		$usetengine = \CI::$APP->tengine->config('active');
		$exttengine = \CI::$APP->tengine->config('fileext');
		$exts = array('');
		$ext = pathinfo($view, PATHINFO_EXTENSION);
		// untuk php tidak perlu pakai TEngine
		if ($ext == 'php') {
			$usetengine = FALSE;
		}elseif ($usetengine && empty($ext)) {
			$exts = array('', EXT, ".$exttengine");
		}
		
		$search = array('views/', 'themes/global/', '');
		if (isset($this->theme->renderer)) {
			// array_unshift($search, 'themes/' . $this->theme->renderer->nama_res . '/');
			$this->theme->renderer->setViewPaths($search);
		}
		foreach ($search as $base) {
			foreach ($exts as $ext) {
				list($path, $_view) = \Modules::find($view.$ext, $module, $base);
				if ($path != FALSE) {
					$_ext = !$ext && is_file($path.$_view.EXT) ? EXT : $ext;
					break;
				}
			}

			if ($path != FALSE) break;
		}

		//log_message('debug', "SEARCH PATH " . print_r(['search' => $search, 'exts' => $exts, 'view' => $view, 'module' => $module, 'path' => $path, 'ext' => $_ext], TRUE));

		// kembalikan konteks module ke module semula
		if(isset($tmp_module))
			$this->_module = $tmp_module;

		return $path != FALSE ? array($path, $_view, $_ext) : array(FALSE, FALSE, FALSE);
	}

	public function switch_module($module) {
		if ($module == $this->_module)
			return $this->_module;
		$modul_asli = $this->_module;
		\CI::$APP->router->locate(explode('/', $module));
		/* set the module name */
		$this->_module = \CI::$APP->router->fetch_module();
		return $modul_asli;
	}

	public function prepare_view_path()
	{
		$CI =& \get_instance();

		// Muat terlebih dahulu supaya bisa menggunakan base_url atau base_path pada theme
		$this->helper(array('url', 'xpath'));

		// ambil nama class untuk menentukan folder-folder terkait view yang ada 
		// didalam modul (themes/{compui,element,errors,scripts,views}/, views/)
		// load semua config 'theme' baik yang ada di APPPATH maupun di module jika tersedia
		$CI->config->load('theme', FALSE, FALSE, $this->_module, TRUE);
		$this->nama_theme = strtolower($CI->config->item('theme_class'));

		$path = APPPATH.'themes/'.  strtolower($this->nama_theme).'/';
		if (!isset($this->_ci_view_paths[$path])) {
			// override tempat views di subfolder themes, langsung dibahwa APPPATH dan views/nama_theme/
			$this->_ci_view_paths = array($path => TRUE) +
				$this->_ci_view_paths;// +
				//array(APPPATH => TRUE);// +
				//array(APPPATH.'views/'.  strtolower($nama).'/' => TRUE);
			$this->_ci_view_paths = $this->_ci_view_paths + array(APPPATH => TRUE);
			log_message('debug', ' === >> View Paths: ' . print_r($this->_ci_view_paths, true));
		}
	}

	public function add_view_path($path, $view_cascade = TRUE, $suffix = TRUE) {
		$this->_ci_view_paths = $suffix === FALSE ? $this->_ci_view_paths + array($path.$suffix => $view_cascade) : array($path.$suffix => $view_cascade) + $this->_ci_view_paths;
	}

	/**
	 * OVERRIDE MX/Loader agar file view bisa mendukung html templating engine
	 *
	 * @param	array
	 * @return	void
	 */
	public function _ci_load($_ci_data) {
		
		extract($_ci_data);
		$usetengine = FALSE;
		if (isset($_ci_view)) {
			$usetengine = \CI::$APP->tengine->config('active');
			$exttengine = \CI::$APP->tengine->config('fileext');
			
			$_ci_path = '';

			$_ci_file = $_ci_view;
			//log_message('info', 'PARAMS VIEW: ' . print_r(array('ci_file' => $_ci_file, 'ci_ext' => $_ci_ext, 'usetengine' => $usetengine, 'view_paths' => $this->_ci_view_paths), TRUE));
			foreach ($this->_ci_view_paths as $path => $cascade) {
				foreach ($_ci_ext as $ext) {
					if (!is_dir($path.$_ci_file.$ext) && file_exists($view = $path.$_ci_file.$ext)) {
						$_ci_path = $view;
						if ($usetengine) {
							$_t_path = $path;
							$_t_view = $_ci_file.$ext;
							$usetengine = $usetengine === TRUE && pathinfo($_t_view, PATHINFO_EXTENSION) == $exttengine;
						}
						break;
						//log_message('info', 'Ditemukan file TEngine: ' . $_ci_path . '. Pakai TEngine: ' . $usetengine);
					}
				}

				if ($_ci_path) break;
				
				if ( ! $cascade) break;
			}
			
		} elseif (isset($_ci_path)) {
			
			$_ci_file = basename($_ci_path);
			if( ! file_exists($_ci_path)) $_ci_path = '';
		}

		if (empty($_ci_path)) 
			show_error('Unable to load the requested file: '.$_ci_file);

		if (isset($_ci_vars)) 
			$this->_ci_cached_vars = array_merge($this->_ci_cached_vars, (array) $_ci_vars);
		
		if ($usetengine) {
			$return = '';
			\CI::$APP->tengine->directories($_t_path);
			try {
				$return = \CI::$APP->tengine->view($_t_view, $this->_ci_cached_vars, $_ci_return);

				log_message('debug', 'TEngine File loaded: '.$_t_view.'('.$_t_path.')');
			} catch (\Haanga_Compiler_Exception $e) {
				_exception_handler(E_USER_ERROR, $e->getMessage(), $_t_path . $_t_view, $e->getLine());
			} catch (\Exception $e) {
				_exception_handler(E_USER_ERROR, $e->getMessage(), $_t_path . $_t_view, 0);
			}

			if ($_ci_return == TRUE) return $return;
		}else {
			extract($this->_ci_cached_vars);

			ob_start();

			if ((bool) @ini_get('short_open_tag') === FALSE AND \CI::$APP->config->item('rewrite_short_tags') == TRUE) {
				echo eval('?>'.preg_replace("/;*\s*\?>/", "; ?>", str_replace('<?=', '<?php echo ', file_get_contents($_ci_path))));
			} else {
				include($_ci_path); 
			}

			log_message('debug', 'Native File loaded: '.$_ci_path);

			if ($_ci_return == TRUE) return ob_get_clean();

			if (ob_get_level() > $this->_ci_ob_level + 1) {
				ob_end_flush();
			} else {
				\CI::$APP->output->append_output(ob_get_clean());
			}
		}
	}

	public function executor($eksekutor)
	{
		Manager::loadExecutor($eksekutor);
	}

	public function permission($perm, $inspect = true, $params = null)
	{
		$CI =& get_instance();
		if (Manager::status() == Manager::SETUP_SUCCESS) {
			// Permission::getInspector() hanya tersedia untuk \WebCore\Session\Identity\Permission
			if (is_array($perm)) {
				$module = isset($perm['modul']) ? $perm['modul'] : null;
				$perm = isset($perm['plugin']) ? $perm['plugin'] : null;
			}else {
				$arr = explode('@', $perm);
				$perm = $arr[0];
				$module = count($arr) > 1 ? $arr[1] : null;
			}
			
			$inspector = $CI->permission->getInspector($perm);
			if (!$inspector) {
				$inspector = $this->custom($perm, 'permissions', 'Permission', $module);
				$CI->permission->addInspector($perm, $inspector);
			}
		}
		if ($inspect)
			$CI->permission->inspect($perm, $params);
	}

	public function custom($class, $folder, $suffix, $module = null, $excludes = array())
	{
		static $_classes = array();
		if (isset($_classes[$folder][$class]))
			return $_classes[$folder][$class];

		$name = str_replace(' ', '', ucwords(str_replace('_', ' ', $class))) . $suffix;

		if (!class_exists($name)) {
			if (!in_array($name, $excludes)) {
				if ($path = $this->cari($folder . '/', $class . '.php', $module)) { // handle HMVC
					require $path;
				}
			}
		}
		
		if (!class_exists($name))
			throw new \Exception("Class $name tidak ditemukan di $class.php", 121);

		$_classes[$folder][$class] = new $name();
		return $_classes[$folder][$class];
	}
}
