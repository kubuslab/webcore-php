<?php
namespace WebCore\Inisiator\CI3;

use WebCore\System\PathFinderInterface;

class PathFinder implements PathFinderInterface
{
    public function find(string $class, string $folder, string $suffix, string $module = null, bool $required = false) : array
    {
        $ci =& \get_instance();

        $name = str_replace(' ', '', ucwords(str_replace('_', ' ', $class))) . $suffix;
        if ($path = $ci->load->cari($folder . '/', $class . '.php', $module)) { // handle HMVC
            if ($required)
                require $path;
            return [$path, $name];
        }

        return [false, $name];
    }
}
