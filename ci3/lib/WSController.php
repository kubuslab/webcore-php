<?php
namespace WebCore\Inisiator\CI3;

use DateTime;
use DateTimeZone;

use WebCore\Inisiator\CI3\Exceptions\IntegritasInputException;

abstract class WSController extends \REST_Controller {
	/**
	 * @var DateTimeZone
	 */
	public $serverTimezone;

	/**
	 * @var DateTime
	 */
	public $serverTime;

	/**
	 * @var string
	 */
	public $serverOffsetText;

	/**
	 * @var DateTimeZone
	 */
	public $clientTimezone;

	/**
	 * @var \Messages
	 */
	public $messages;

	/**
	 * @var \Session
	 */
	public $session;

	/**
	 * @var \Usession
	 */
	public $usession;
	
	/**
	 * @var \Permission
	 */
	public $permission;
	
	/**
	 * @var \Variable
	 */
	public $variable;
	
	/**
	 * @var \Web_Hooks
	 */
	public $syshooks;

	/**
	 * @var \Web_CustomHooks
	 */
	public $hooks;

	/**
	 * @var \Memobjek
	 */
	public $memobjek;

	/**
	 * @var \Features
	 */
	public $features;
	
	/**
	 * @var \Profiler
	 */
	public $profiler;

	//region Helper Object Auto-Complete untuk IDE
	/**
	 * Object Helper Autocomplete untuk Formcm
	 * @var \Formcm
	*/
	public $formcm;

	/**
	 * Object Helper Autocomplete untuk Web_Loader
	 * @var WebLoader
	*/
	public $load;

	/**
	 * Object Helper Autocomplete untuk CI_DB_query_builder
	 * @var \CI_DB_query_builder
	 */
	public $db;

	/**
	 * Object Helper Autocomplete untuk Web_Config
	 * @var WebConfig
	 */
	public $config;

	/**
	 * Object Helper Autocomplete untuk Web_Router
	 * @var WebRouter
	 */
	public $router;

	/**
	 * Object Helper Autocomplete untuk CI_URI
	 * @var \CI_URI
	 */
	public $uri;

	/**
	 * Object Helper Autocomplete untuk CI_Output
	 * @var \CI_Output
	 */
	public $output;

	/**
	 * Object Helper Autocomplete untuk CI_Input
	 * @var \CI_Input
	 */
	public $input;

	/**
	 * Object Helper Autocomplete untuk CI_Security
	 * @var \CI_Security
	 */
	public $security;

	/**
	 * Object Helper Autocomplete untuk CI_Lang
	 * @var \CI_Lang
	 */
	public $lang;

	/**
	 * Object Helper Autocomplete untuk CI_Utf8
	 * @var \CI_Utf8
	 */
	public $utf8;
	//endregion

	protected $cek_token = FALSE;
	protected $oto_hapus = TRUE;
	protected $ada_error = FALSE;
	protected $rest2 = NULL;
	protected $ceksum = NULL;
	protected $cekpk = NULL;
	
	var $controllerType = 'WS';

	public function __construct() {
		parent::__construct();
		log_message('debug', "\n\n\t>> MENGAKSES  WS " . $this->router->fetch_directory() . '' . $this->router->fetch_class() . '/' . $this->router->fetch_method() . " " . $this->router->request_method() . " <<\n\t\t--> " . $_SERVER['REQUEST_URI'] . " \n\n");

		try {
			// TAMBAHKAN KEMAMPUAN DATA CACHE MANAGER
			$this->load->library('datacm');
			
			// masukkan juga System Hooks
			$this->syshooks =& load_class('Hooks', 'core');

			// siapkan custom hooks untuk berbagai kebutuhan
			$this->hooks =& load_class('CustomHooks', 'core');
			
			////// mulai proses bootstrap //////
			// ORM Profiler
			$this->load->library('profiler/Profiler', 'profiler');
			
			// siapkan modul variable
			$this->load->library('variable');
			
			// FIXED: karena sess_update membutuhkan library datacm dan datacm membutuhkan variable, maka load session disini
			$this->load->library('session');
			$this->session->start();
			
			// load MemObjek sebenarnya usdah dilakukan di session, ini hanya opsional saja
			$this->load->library('Datacm/memobjek', NULL, 'memobjek');
			
			// load user session
			$this->load->library('usession');
			$this->usession->init();

			// load features
			$this->load->library('features');
			$this->features->build();

			// load permission
			$this->load->library('permission');
			$this->permission->inspect('user');

			// jalankan unhooks, System Hooks dan Custom Hooks
			$this->syshooks->unhook();
			$this->hooks->unhook();
		} catch (\Exception $e) {
			// tanggapi error nanti di controller fungsional spesifik
			//ERROR Session time out
			//ERROR Tidak diperbolehkan mengakses resource
			//ERROR daftar permission tidak ditemukan
			$this->tanggapi_error($e);
		}
	}

	public function data($key, $default = null)
	{
		$data = $this->context->data();
		$d = $data[$key];
		if (isset($d))
			return $d;

		return $default;
	}

	public function setData($key, $value)
	{
		$data = $this->context->data();
		$data[$key] = $value;
	}

	public function getResponseFormat()
	{
		return $this->response->format;
	}

	public function setResponseFormat($format)
	{
		$this->response->format = $format;
	}

	public function response_text($response, $http_code = null, $format = 'txt') {
		// paksa output ke plain/text
		$this->response->format = $format;
		$this->response($response, $http_code);
	}
	
	public function response_message($status, $message, $msg_code = null, $data = null, $http_code = null) {
		$ret = $this->create4ws($message, $status, $msg_code, $data);
		$this->response($ret, $http_code);
	}
	
	public function response_status($status) {
		$this->response(array('status' => $status));
	}

	public function response_success($message, $data = array(), $external = true) {
		if ($external) {
			$ret = $this->create4ws($message, 'success');
			$ret += is_object($data) ? (array) $data : $data;
			$this->response($ret, 200);
		}else
			$this->response_message('success', $message, null, $data, 200);
	}
	
	public function response($data = array(), $http_code = null)
	{
		if (!empty($this->cek_token) && $this->oto_hapus && !$this->ada_error)
			$this->hapus_cache();
		if (is_array($data)) {
			if (isset($data['status']) && $data['status']== false && !empty($data['error']))
				$data = array('message' => array('tipe' => 'error', 'pesan' => $data['error'], 'kode' => 10));
			
			if (isset($this->php_errors) && !empty($this->php_errors))
				$data['exceptions'] = $this->php_errors;
		}
		parent::response($data, $http_code);
	}
	
	public function _remap($object_called, $arguments) {
		try {
			parent::_remap($object_called, $arguments);
		} catch (\Exception $e) {
			$this->tanggapi_error($e);
		}
	}

	public function show_error()
	{
		$this->_404_message = 'The page you requested was not found.';

		$this->load->config('webexception', FALSE, FALSE, null, TRUE);
		$cfg = $this->config->item('webexception');
		foreach (array(/* 'backlink_title', 'backlink_path', '_404_heading', */ '_404_message') as $key) {
			if (isset($cfg[$key]))
				$this->$key = $cfg[$key];
		}

		$err = $this->create4ws($this->_404_message, 'error', 404);
		if ($this->response->format == 'html')
			$this->response->format = 'json';
		$this->ada_error = TRUE;
		$this->response($err, 503);
	}

	/**
	 * @param \Exception $e
	 */
	function tanggapi_error($e, $http_code = null) {
		$err = $this->create4ws($e); // standarisasi message
		log_message('debug', " ==== BACKTRACE ERROR ==== \n" . print_r($err, true) . ' -- ' . $e->getTraceAsString());
		if ($this->response->format == 'html')
			$this->response->format = 'json';
		if ($e instanceof \HttpException && !isset($http_code))
			$http_code = $e->getHttpCode();
		$this->ada_error = TRUE;
		$this->response($err, $http_code);
	}

	function create4ws($message, $type = 'message', $code = null, $data = null) {
		if ($message instanceof \Exception) {
			$type = 'error';
			$code = $message->getCode();
			$message = $message->getMessage();
		}

		$return = array('tipe' => $type, 'pesan' => $message);
		if (isset($code)) $return['kode'] = $code;
		if (!empty($data))
			$return['data'] = $data;
		return array( 'message' => $return );
	}

	function get_api_object() {
		return $this->rest;
	}

	protected function _detect_api_key() {
		// Get the api key name variable set in the rest config file
		$api_key_variable = config_item('rest_key_name');
		$api_key_variable_param = config_item('rest_key_name_param');

		// Work out the name of the SERVER entry based on config
		$key_name = 'HTTP_'.strtoupper(str_replace('-', '_', $api_key_variable));
		if (($key = isset($this->_args[$api_key_variable]) ? $this->_args[$api_key_variable] : 
			(isset($this->_args[$api_key_variable_param]) ? $this->_args[$api_key_variable_param] : $this->input->server($key_name)))) {

			log_message('debug', "\n\n ==== API KEY --> '$key' ====\n");
			// load MemObjek lebih awal
			$this->load->library('Datacm/memobjek', NULL, 'memobjek');

			// Cek dari MemObjek UserSession:API dulu sebelum memeriksa database
			$memkey = 'UserSession:API:' . $key;
			$this->memobjek->load($memkey, FALSE, NULL);
			$row = $this->memobjek->get($memkey, 'API');
			if (!is_array($row)) {
				$row = parent::_detect_api_key();
				if (!is_object($row)) {
					if (isset($this->_args[$api_key_variable_param]) && $key == $this->_args[$api_key_variable_param]) {
						$row = $this->check_api_key($key);
					}else
						return $row;
				}
				if (empty($row->active))
					return FALSE;

				log_message('debug', "\n\n ==== OBJECT USER --> '$key' ====\n" . print_r($row, TRUE));

				$row = (array) $row;

				// cek keberadaan user_id
				$user = $this->db->where('id', $row['user_id'])->get('users')->row_array();
				if (!empty($user)) {
					$row['user_ada'] = TRUE;
					$row['user_aktif'] = intval($user['aktif']) == 1;

					$this->rest->user_ada = $row['user_ada'];
					$this->rest->user_aktif = $row['user_aktif'];
				}else {
					$row['user_ada'] = FALSE;
					$this->rest->user_ada = $row['user_ada'];
				}

				$this->memobjek->set($memkey, 'API', $row, NULL, NULL);
				$this->memobjek->duration($memkey, config_item('rest_memobjek_expiration'));

				if (!$row['user_ada'] || !$row['user_aktif'])
					return FALSE;
			}else {
				$this->rest->key = $row[config_item('rest_key_column')];
				$this->rest->level = NULL;
				$this->rest->user_id = NULL;
				$this->rest->ignore_limits = FALSE;

				isset($row['user_id']) AND $this->rest->user_id = $row['user_id'];
				isset($row['level']) AND $this->rest->level = $row['level'];
				isset($row['ignore_limits']) AND $this->rest->ignore_limits = $row['ignore_limits'];

				if (isset($row['user_ada'])) {
					$this->rest->user_ada = $row['user_ada'];
					$this->rest->user_aktif = $row['user_aktif'];
				}else
					$this->rest->user_ada = $row['user_ada'];

				/*
				 * If "is private key" is enabled, compare the ip address with the list
				 * of valid ip addresses stored in the database.
				 */
				if(!empty($row['is_private_key']))
				{
					// Check for a list of valid ip addresses
					if(isset($row['ip_addresses']))
					{
						// multiple ip addresses must be separated using a comma, explode and loop
						$list_ip_addresses = explode(",", $row['ip_addresses']);
						$found_address = FALSE;
						
						foreach($list_ip_addresses as $ip_address)
						{
							if($this->input->ip_address() == trim($ip_address))
							{
								// there is a match, set the the value to true and break out of the loop
								$found_address = TRUE;
								break;
							}
						}
						
						return $found_address;
					}
					else
					{
						// There should be at least one IP address for this private key.
						return FALSE;
					}
				}

				if (!$row['user_ada'] || !$row['user_aktif'])
					return FALSE;
			}

			//log_message('debug', $memkey . ' --- ' . print_r($row, TRUE));
			return $row;
		}

		return TRUE;
	}
	
	protected function check_api_key($key)
	{
		if ( ! ($row = $this->rest->db->where(config_item('rest_key_column'), $key)->get(config_item('rest_keys_table'))->row()))
		{
			return FALSE;
		}

		$this->rest->key = $row->{config_item('rest_key_column')};

		$usercol   = config_item('rest_user_id_column');
		$levelcol  = config_item('rest_level_column');
		$ignorecol = config_item('rest_ignore_limits_column');
		$activecol   = config_item('rest_active_column');

		if (isset($row->$usercol))
		{
			$row->user_id = $row->$usercol;
			$this->rest->user_id = $row->$usercol;
			unset($row->$usercol);
		}

		if (isset($row->$levelcol))
		{
			$row->level = $row->$levelcol;
			$this->rest->level = $row->$levelcol;
			unset($row->$levelcol);
		}

		if (isset($row->$ignorecol))
		{
			$row->ignore_limits = $row->$ignorecol;
			$this->rest->ignore_limits = $row->$ignorecol;
			unset($row->$ignorecol);
		}

		if (isset($row->$usercol))
		{
			$row->active = $row->$activecol;
			unset($row->$activecol);
		}

		/*
			* If "is private key" is enabled, compare the ip address with the list
			* of valid ip addresses stored in the database.
			*/
		if(isset($row->is_private_key) && !empty($row->is_private_key))
		{
			// Check for a list of valid ip addresses
			if(isset($row->ip_addresses))
			{
				// multiple ip addresses must be separated using a comma, explode and loop
				$list_ip_addresses = explode(",", $row->ip_addresses);
				$found_address = FALSE;
				
				foreach($list_ip_addresses as $ip_address)
				{
					if($this->input->ip_address() == trim($ip_address))
					{
						// there is a match, set the the value to true and break out of the loop
						$found_address = $row;
						break;
					}
				}
				
				return $found_address;
			}
			else
			{
				// There should be at least one IP address for this private key.
				return FALSE;
			}
		}
		
		return $row;
	}
	
	protected function early_checks() {
		$this->load->config('rest2');
		if (config_item('rest2_enable_integrity')) {
			$this->cek_integritas();
		}
	}
	
	protected function cek_integritas() {
		try {
			// ambil ceksum dan public key
			if ($this->_args['ceksum'] && $this->_args['cekpk']) {
				$this->ceksum = $this->_args['ceksum'];
				$this->cekpk = $this->_args['cekpk'];
				unset($this->_args['ceksum']);
				unset($this->_args['cekpk']);
				if (isset($this->_get_args['ceksum']))
					unset($this->_get_args['ceksum']);
				if (isset($this->_get_args['cekpk']))
					unset($this->_get_args['cekpk']);
			}elseif (isset($this->request->body['cek']) && $this->request->body['cek']['sum'] && $this->body['cek']['pk']) {
				$this->ceksum = $this->request->body['cek']['sum'];
				$this->cekpk = $this->request->body['cek']['pk'];
				unset($this->request->body['cek']);
			} else {
				// error
				throw new IntegritasInputException('key data tidak ditemukan', 1);
			}

			if ( ! ($row = $this->db->where('public_key', $this->cekpk)->get(config_item('rest2_table_integrity'))->row())) {
				// data tidak memenuhi integrity check
				throw new IntegritasInputException('key data tidak dikenali', 2);
			}

			// cek integritas data dengan hash_hmac SHA256
			if ($this->request->method == 'get') {
				if (count($this->_get_args) == 0) {
					throw new IntegritasInputException('Integritas data lemah', 1);
				}
				$hash = hash_hmac("sha256", json_encode ($this->_get_args), $this->cekpk);
			}else
				$hash = hash_hmac("sha256", json_encode ($this->request->body), $this->cekpk);
			if ($this->ceksum != $hash) {
				// ceksum data tidak sesuai
				throw new IntegritasInputException('Integritas data dipertanyakan', 3);
			}
		} catch (IntegritasInputException $e) {
			$this->tanggapi_error($e);
		}
	}
	
	function hapus_token_otomatis($otomatis) {
		$this->oto_hapus = $otomatis !== FALSE;
	}

	function cek_token($token = null) {
		$this->load->library('formcm');
		$this->form_token = $token ? $token : (isset($this->_args['token']) ? $this->_args['token'] : null);
		// TANDAI BAHWA cek_token telah dipanggil
		$this->cek_token = TRUE;
		return $this->formcm->cekToken($this->form_token);
	}
	
	function get_cache() {
		$token = isset($this->form_token) ? $this->form_token : (isset($this->_args['token']) ? $this->_args['token'] : NULL);
		if (!$token) return;
		$this->load->library('formcm');
		return $this->formcm->getCache($token);
	}
	
	function hapus_cache() {
		$token = isset($this->form_token) ? $this->form_token : (isset($this->_args['token']) ? $this->_args['token'] : NULL);
		if (!$token) return;
		$this->load->library('formcm');
		$this->formcm->clearCache($token);
	}
	
	function ambil_input($sumber = null, $formid = null, $default = array(), $transparan = FALSE) {
		if (empty($sumber))
			$sumber = $this->post();
		if (empty($formid)) {
			$cache = $this->get_cache();
			$formid = $cache['formid'];
		}
		return $this->petakan_data($sumber, $formid, false, $default, $transparan);
	}
	
	/**
	 * Mengambil sub-input yang berisi array list dengan struktur data yang seragam pada data $sumber
	 */
	function ambil_subinput_array($sumber = null, $formid = null, $default = array(), $transparan = FALSE) {
		if (empty($sumber))
			$sumber = $this->post();
		if (empty($formid)) {
			$cache = $this->get_cache();
			$formid = $cache['formid'];
		}
		return $this->petakan_subdata_array($sumber, $formid, false, $default, $transparan);
	}
	
	function petakan_data($sumber, $formid, $dibalik = false, $default = array(), $transparan = FALSE) {
		log_message('debug', 'PETAKAN DATA "' . $formid . '"');
		if (is_array($formid)) {
			if ($dibalik)
				$formid = array_flip ($formid);
			return ambil_http_input($sumber, $formid, false, $default);
		} else {
			$this->load->config('forms');
			// $map = config_item('forms_cache_mapping');
			/** @var array */
			$map = $this->config->item('forms_cache_mapping');
			if (isset($map[$formid])) {

				if ($dibalik)
					$map[$formid] = array_flip ($map[$formid]);
				return ambil_http_input($sumber, $map[$formid], false, $default);
			}else return $transparan ? $sumber : array();
		}
	}

	function petakan_subdata_array($sumber, $formid, $dibalik = false, $default = array(), $transparan = FALSE) {
		log_message('debug', 'PETAKAN DATA "' . $formid . '"');
		if (is_array($formid)) {
			if ($dibalik)
				$formid = array_flip ($formid);
			return ambil_http_subinput_array($sumber, $formid, false, $default);
		} else {
			$this->load->config('forms');
			// $map = config_item('forms_cache_mapping');
			/** @var array */
			$map = $this->config->item('forms_cache_mapping');
			if (isset($map[$formid])) {

				if ($dibalik)
					$map[$formid] = array_flip ($map[$formid]);
				return ambil_http_subinput_array($sumber, $map[$formid], false, $default);
			}else return $transparan ? $sumber : array();
		}
	}
	
	static function flatten($array, $key = '') {
		if (!is_array($array)) {
			// nothing to do if it's not an array
			return $array;
		}

		$key = !empty($key) ? $key . ':' : $key;
		$result = array();
		foreach ($array as $k => $value) {
			// explode the sub-array, and add the parts
			$result[$key . $k] = self::flatten($value, $key . $k);
		}

		return $result;
	}

	public function manager($fungsi, $params = array())
	{
		if (Manager::status() != Manager::SETUP_SUCCESS)
			return NULL;

		if (is_array($params) && count($params) > 0)
			return call_user_func_array(array(Manager::class, $fungsi), $params);
		else
			return call_user_func(Manager::class, $fungsi);
	}

	public function args($key = null)
	{
		if (!isset($key)) return $this->_args;
		return isset($this->_args[$key]) ? $this->_args[$key] : null;
	}
}
