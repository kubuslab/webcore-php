<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['logger']['channels']['cache']['debug'] = FALSE;
$config['logger']['channels']['cache']['info'] = FALSE;
$config['logger']['channels']['form'] = FALSE;