<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cache']['adapter'] = 'memcache';
$config['cache']['enabled'] = TRUE;

// Untuk adapter Memcache
$config['cache']['memcache'] = array(
    'servers' => array(
        array(
            'hostname' => '10.130.13.144',
            'port' => 11211,
            'weight' => 1,
        ),
    ),
    'defaultSerializer' => 'Json',
    'lifetime' => 3600,
    'serializer' => null,
    'prefix' => ''
);

// Untuk adapter REDIS
$config['cache']['redis'] = array();